﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using GraphInterviewPractice.InterviewProblems;
using System.Collections.Generic;

namespace Miscellaneous.Algorithms.Tests
{
    [TestClass()]
    public class MineSweeperTests
    {
        [TestMethod()]
        public void MineSweeperTest()
        {
            int[,] minefield =  { { 0,1,0,1},
                                  { 0,0,1,1},
                                  { 0,0,0,0},
                                  { 1,1,0,0}
            };

            MineSweeper ms = new MineSweeper();
            IList<MineSweeper.Result> res = ms.Solve(minefield);
            Assert.AreEqual(res.Count, 6);
        }
    }
}