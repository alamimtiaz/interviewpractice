﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using GraphInterviewPractice.InterviewProblems;

namespace GraphInterviewPractice.Algorithm.Tests
{
    [TestClass()]
    public class FindLargestRegionTests
    {
        [TestMethod()]
        public void FindLargestRegionTest()
        {
            int[,] regions =  { { 0,0,1,1,0},
                                  { 1,0,1,1,0},
                                  { 0,1,0,0,0},
                                  { 0,0,0,0,1}
            };


            FindLargestRegion ms = new FindLargestRegion();
            int res = ms.Solve(regions);
            Assert.AreEqual(6, res);
        }
    }
}