﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

using GraphInterviewPractice.InterviewProblems;

namespace Graph.Algorithm.Tests
{
    [TestClass()]
    public class SumOfDependenciesTests
    {
        [TestMethod()]
        public void FindSumOdDependencies()
        {
            List<int>[] adj = new List<int>[4];
            adj[0] = new List<int>();
            adj[1] = new List<int>();
            adj[2] = new List<int>();

            adj[0].Add(2);
            adj[0].Add(3);
            adj[1].Add(3);
            adj[2].Add(3);

            SumOfDependencies sd = new SumOfDependencies();
            int res = sd.Solve(adj);
            Assert.AreEqual(4, res);
        }
    }
}