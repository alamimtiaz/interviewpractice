﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatrixApplication.InterviewProblems
{
    /* Find the number of negative numbers in a column-wise / row-wise sorted matrix M[][].
     * Suppose M has n rows and m columns.
     * 
     * Example:
        Input:  M =  [-3, -2, -1,  1]
                     [-2,  2,  3,  4]
                     [4,   5,  7,  8]
        Output : 4
        We have 4 negative numbers in this matrix
    */

    public class TwoDimensionArray
    {
        public int GetCountOfNegativeNumbers(int[,] M, int n, int m)
        {
            int count = 0;
            int i = 0;
            int j = m - 1;
            while(i < n && j >= 0)
            {
                if(M[i,j] < 0)
                {
                    count += (j + 1);
                    i++;
                }
                else
                {
                    j--;
                }
            }

            return count;
        }
    }
}
