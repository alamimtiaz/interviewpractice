﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TreeInterviewPractice.Model;

namespace TreeInterviewPractice.DataStructures.Tests
{
    [TestClass()]
    public class BinarySearchTreeTests
    {
        [TestMethod()]
        public void HasPathSumTest()
        {
            Node treeNode = new Node(5);
            treeNode.Left = new Node(4);
            treeNode.Left.Left = new Node(1);
            treeNode.Right = new Node(3);
            treeNode.Right.Right = new Node(8);

            BinarySearchTree bst = new BinarySearchTree();

            Assert.AreEqual(true, bst.HasPathSum(treeNode, 16));
        }
    }
}