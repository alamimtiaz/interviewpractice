﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TreeInterviewPractice.DataStructures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreeInterviewPractice.DataStructures.Tests
{
    [TestClass()]
    public class TriesTests
    {
        [TestMethod()]
        public void InsertTest()
        {
            Tries t = new Tries();
            t.Insert("imtiaz");
            t.Insert("important");

            bool result = t.StartsWith("imti");
            Assert.AreEqual(true, result);

            result = t.StartsWith("alam");
            Assert.AreEqual(false, result);

            result = t.StartsWith("important");
            Assert.AreEqual(true, result);
        }
    }
}