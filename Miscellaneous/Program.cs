﻿using System;

using Miscellaneous.Algorithms;

namespace Miscellaneous
{
    class Program
    {
        static void Main(string[] args)
        {
            PrintAllPossibleWordsFromPhoneNumbers p = new PrintAllPossibleWordsFromPhoneNumbers();
            p.Solve(new int[] {2, 5, 2, 6 });
        }

    }
}
