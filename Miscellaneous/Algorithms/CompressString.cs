﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Miscellaneous.Algorithms
{
    class CompressString
    {
        // Need refactorin O N/2 solution
        public String Solve(String input)
        {
            if (string.IsNullOrWhiteSpace(input))
                return input;

            char[] chars = input.ToLower().ToCharArray();
            StringBuilder sb = new StringBuilder();
            int sum = 1;
            for (int i = 0; i < chars.Length - 1; i++)
            {
                if(chars[i] == chars[i+1])
                {
                    sum++;
                }
                else
                {
                    sb.Append(chars[i]);
                    sb.Append(sum);
                    sum = 1;
                }
            }
            sb.Append(chars[chars.Length - 1]);
            sb.Append(sum);

            if (sb.Length > input.Length) return input;

            return sb.ToString();
        }
    }
}
