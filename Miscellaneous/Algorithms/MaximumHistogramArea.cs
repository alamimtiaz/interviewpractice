﻿using System.Collections.Generic;

namespace Miscellaneous.Algorithms
{
    public class MaximumHistogramArea
    {
        public int FindMaximumRectangleArea(int[] input)
        {
            Stack<int> stack = new Stack<int>();
            int maxArea = 0;
            int area = 0;
            int i;

            for(i = 0; i < input.Length; i++)
            {
                if (stack.Count == 0 || input[stack.Peek()] <= input[i])
                {
                    stack.Push(i);
                }
                else
                {
                    int top = stack.Pop();
                    if(stack.Count == 0)
                    {
                        area = input[top] * i;
                    }
                    else
                    {
                        area = input[top] * (i - stack.Peek() - 1);
                    }

                    if (area > maxArea) maxArea = area;
                }
            }

            while(stack.Count != 0)
            {
                int top = stack.Pop();
                if (stack.Count == 0)
                {
                    area = input[top] * i;
                }
                else
                {
                    area = input[top] * (i - stack.Peek() - 1);
                }

                if (area > maxArea) maxArea = area;
            }

            return maxArea;
        }
    }
}
