﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Miscellaneous.Algorithms
{
    public class NumberOccurrence
    {
        public int GetOccurrence(int k, int[] numbers, int startIndex, int endIndex)
        {
            if (endIndex < startIndex) return 0;
            if (numbers[startIndex] > k) return 0;
            if (numbers[endIndex] < k) return 0;
            if (numbers[startIndex] == k && numbers[endIndex] == k) return (startIndex + endIndex + 1);

            int midIndex = (startIndex + endIndex) / 2;
            if (numbers[midIndex] == k) // got the number at mid point, and it may occur before and after as well
                return 1 + GetOccurrence(k, numbers, startIndex, midIndex - 1) + GetOccurrence(k, numbers, midIndex + 1, endIndex);
            else if (numbers[midIndex] > k) // middle number is high, then k can be found in left side
                return GetOccurrence(k, numbers, startIndex, midIndex - 1);
            else // middle number is small, then k can be found in right side
                return GetOccurrence(k, numbers, midIndex + 1, endIndex);
        }
    }
}
