﻿using System.Collections.Generic;

namespace Miscellaneous.Algorithms
{
    public class BalancedParantheses
    {
        public char[,] TOKENS = new char[3, 2]
        {
            { '{', '}' },
            { '[', ']' },
            { '(', ')' }
        };

        public bool IsBalanced(string expression)
        {
            Stack<char> stack = new Stack<char>();
            foreach(char ch in expression.ToCharArray())
            {
                if(IsOpenTerm(ch))
                {
                    stack.Push(ch);
                }
                else
                {
                    if (stack.Count == 0 || !Matches(stack.Pop(), ch))
                    {
                        return false;
                    }
                }
            }

            return stack.Count == 0;
        }

        private bool IsOpenTerm(char ch)
        {
            for(int i = 0; i < TOKENS.GetLength(0); i++)
            {
                if(ch == TOKENS[i,0])
                {
                    return true;
                }
            }

            return false;
        }

        private bool Matches(char openTerm, char closeTerm)
        {
            for (int i = 0; i < TOKENS.GetUpperBound(0); i++)
            {
                if (openTerm == TOKENS[i, 0])
                {
                    return TOKENS[i,1] == closeTerm;
                }
            }

            return false;
        }
    }
}
