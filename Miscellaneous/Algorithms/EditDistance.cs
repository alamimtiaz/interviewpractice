﻿using System;

namespace Miscellaneous.Algorithms
{
    //Given two strings of size m, n and set of operations replace (R), insert (I) and delete (D) 
    //all at equal cost. Find minimum number of edits (operations) required to convert one string into another.
    // https://en.wikipedia.org/wiki/Levenshtein_distance

    class EditDistance
    {
        public int Solve(String s1, String s2)
        {
            int n = s1.Length;
            int m = s2.Length;
            int[,] array = new int[n+1, m+1];

            int cost = 0;
            // source prefixes can be transformed into empty string by
            // dropping all characters
            for (int i = 0; i < n; array[i, 0] = i++) ;
            for (int j = 0; j < m; array[0, j] = j++) ;

            for (int i = 1; i <= n; i++)
            {
                for (int j = 1; j <= m; j++)
                {
                    cost = 1;
                    if (s1[i - 1] == s2[j - 1])
                        cost = 0;

                    array[i, j] = Math.Min(
                                        Math.Min(array[i - 1, j] + 1, array[i, j - 1] + 1),
                                         array[i - 1, j - 1] + cost);
                }
            }

            return array[n, m];
        }
    }
}
