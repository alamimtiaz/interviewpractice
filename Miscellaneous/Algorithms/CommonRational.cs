﻿namespace Miscellaneous.Algorithms
{
    public class CommonRational
    {
        /// <summary>
        /// Get Rational Number (ie. input 0.125, output = 1/8)
        /// </summary>
        /// <param name="a"></param>
        /// <returns></returns>
        public string GetRational(double a)
        {
            MathProblems math = new MathProblems();
            int tenPoly = 1;
            while(a * tenPoly - (int)(a * tenPoly) >= .01)
            {
                tenPoly *= 10;
            }

            int divident = (int)(a * tenPoly);
            int divisor = tenPoly;

            int gcd = divident > divisor ? math.GreatestCommonDivisor(divident, divisor) : math.GreatestCommonDivisor(divisor, divident);

            divident /= gcd;
            divisor /= gcd;

            return string.Format("{0} = {1}/{2}", a, divident, divisor);
        }
    }
}
