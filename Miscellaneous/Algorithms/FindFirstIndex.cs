﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Miscellaneous.Algorithms
{
    public class FindFirstIndex
    {
        /// <summary>
        /// To find the first occurence of the number (asumption that the array can have duplicate numbers
        /// O(N)
        /// </summary>
        /// <param name="nums"></param>
        /// <param name="numberToSearch"></param>
        /// <returns></returns>
        public int GetFirstIndex(int[] nums, int numberToSearch)
        {
            int index = -1;

            for(int i = 0; i < nums.Length; i++)
            {
                if(nums[i] == numberToSearch)
                {
                    index = i;
                    break;
                }
            }

            return index;
        }

        /// <summary>
        /// To find the first occurence of the number (consider that numbers are sorted)
        /// O(LgN)
        /// </summary>
        /// <param name="nums"></param>
        /// <param name="numberToSearch"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public int GetFirstIndex(int[] nums, int numberToSearch, int start, int end)
        {
            if (start > end) return -1;
            if (nums[start] > numberToSearch) return -1;
            if (nums[end] < numberToSearch) return -1;
            if (nums[start] == numberToSearch) return start;

            int mid = (start + end) / 2;
            if(nums[mid] == numberToSearch)
            {
                int leftIndex = GetFirstIndex(nums, numberToSearch, start, mid - 1);
                return leftIndex == -1 ? mid : leftIndex;
            }
            else if(nums[mid] > numberToSearch)
            {
                return GetFirstIndex(nums, numberToSearch, start, mid - 1);
            }
            else
            {
                return GetFirstIndex(nums, numberToSearch, mid + 1, end);
            }
        }
    }
}
