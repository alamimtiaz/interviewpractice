﻿namespace Miscellaneous.Algorithms
{
    public class CoinChange
    {
        private static int[] COINS = new int[] { 50, 25, 10, 5, 1 };

        public int Change(int x, int[] coins)
        {
            if (x == 0) return 0;
            int min = x;
            foreach(int coin in coins)
            {
                if(x - coin >= 0)
                {
                    int c = Change(x - coin, coins);
                    if (min > c + 1) min = c + 1;
                }
            }
            return min;
        }
    }
}
