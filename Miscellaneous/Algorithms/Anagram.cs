﻿namespace Miscellaneous.Algorithms
{
    public class Anagram
    {
        // Given two strings (lowercase a->z), how many characters do we need to remove (from either)
        // to make them anagrams

        public static int NUMBER_LETTERS = 26;

        public bool IsAnagram(string s1, string s2)
        {
            if (s1.Length != s2.Length) return false;

            int[] charCounts = new int[NUMBER_LETTERS];

            foreach(char ch in s1.ToLower().ToCharArray())
            {
                int index = ch - 'a';
                charCounts[index]++;
            }

            foreach(char ch in s2.ToLower().ToCharArray())
            {
                int index = ch - 'a';
                charCounts[index]--;
            }

            for(int i = 0; i < charCounts.Length; i++)
            {
                if (charCounts[i] != 0) return false;
            }

            return true;
        }
    }
}
