﻿using System;
using System.Collections.Generic;

namespace Miscellaneous.Algorithms
{
    /*
     * 
        Valid Permutation of Parenthesis
        all combinations of well-formed brackets

        Input : n = 1
        Output: ()

        Input : n = 2
        Output: (()) ()()

        Input : n = 3
        Output: ((())) (()()) (())() ()(()) ()()()


    */
    public class FindCombinationsOfWellFormedBrackets
    {
        public List<String> Solve(int n)
        {
            List<String> result = new List<string>();
            Solve(String.Empty, 0, 0, n, result);
            return result;
        }

        private void Solve(String Output, int open, int close, int n, List<String> result)
        {
            if (open == n && close == n)
            {
                result.Add(Output);
                Console.WriteLine("result: " + Output);
            }
            else
            {
                Console.WriteLine(Output);
                if (open < n)
                {
                        Solve(Output + "(", open + 1, close, n, result);
                }

                if (close < open)
                {
                    Solve(Output + ")", open, close + 1, n, result);
                }
            }

        }
    }
}
