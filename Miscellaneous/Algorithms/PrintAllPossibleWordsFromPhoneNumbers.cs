﻿using System;

namespace Miscellaneous.Algorithms
{
    /*
     * Print all possible words from phone number digits.
     * 
     * For example if input number is 234, possible words which can be formed are (Alphabetical order):
     * adg adh adi aeg aeh aei afg afh afi bdg bdh bdi beg beh bei bfg bfh bfi cdg cdh cdi ceg ceh cei cfg cfh cfi
     */
    class PrintAllPossibleWordsFromPhoneNumbers
    {
        String[] hashTable = { "0", "1", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz" };

        public void Solve(int[] number)
        {
            String[] symbols = new string[number.Length ];
            for(int i=0; i < number.Length; i++)
            {
                symbols[i] = GetCharacters(number[i]);
            }
            PrintAllString(symbols, String.Empty, 0);
        }

        private void PrintAllString(String[] symbols, string result, int n)
        {
            if(n == symbols.Length)
            {
                Console.WriteLine(result);
                return;
            }
       
            for (int i=0; i < symbols[n].Length; i++)
            {
                PrintAllString(symbols, result + symbols[n][i], n + 1);
            }
            
        }


        private String GetCharacters(int n)
        {
            if (n > 9 || n < 0)
                return String.Empty;

            return hashTable[n];
        }

    }

    
}
