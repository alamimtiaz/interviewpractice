﻿using System;

/*
 * Write a program to print all permutations of a given string
 * Below are the permutations of string ABC.
 * ABC ACB BAC BCA CBA CAB
 * 
 
 */

namespace Miscellaneous.Algorithms
{
    class PermuteString
    {
        public void Solve(String s)
        {
            DoWork(s, 0, s.Length-1);
        }

        //* Algorithm Paradigm: Backtracking
        // Time Complexity: n!

        private void DoWork(String s, int l, int r)
        {
            if (l == r)
                Console.WriteLine(s);
            else
                for (int i = l; i <= r; i++)
                {
                    s = swap(s, l, i);      // swap left with index char
                    DoWork(s, l + 1, r);    // Permute
                    s = swap(s, l, i);      // swap it back
                }
        }

        private String swap(String s, int l, int r)
        {
            char[] array = s.ToCharArray();
            char t = array[l];
            array[l] = array[r];
            array[r] = t;

            return new string(array);
        }
    }
}
