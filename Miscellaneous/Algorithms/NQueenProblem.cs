﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// source: https://www.youtube.com/watch?v=xouin83ebxE&t=1s
namespace Miscellaneous.Algorithms
{
    public class NQueenProblem
    {
        public Position[] SolveNQueenOneSolution(int n)
        {
            Position[] positions = new Position[n];
            bool hasSolution = SolveNQueenOneSolutionUntil(n, 0, positions);
            if (hasSolution) return positions;

            return new Position[0];
        }

        public bool SolveNQueenOneSolutionUntil(int n, int row, Position[] positions)
        {
            if (n == row) return true;

            int col;
            for(col = 0; col < n; col++)
            {
                bool foundSafe = true;
                for(int queen = 0; queen < row; queen++)
                {
                    if (positions[queen].Col == col || (positions[queen].Row - positions[queen].Col) == (row - col) ||
                        (positions[queen].Row + positions[queen].Col) == (row + col))
                    {
                        foundSafe = false;
                        break;
                    }
                }

                if(foundSafe)
                {
                    positions[row] = new Position(row, col);
                    if(SolveNQueenOneSolutionUntil(n, row + 1, positions))
                    {
                        return true;
                    }
                }
            }

            return false;
        }
    }

    public class Position
    {
        private int row;
        private int col;

        public Position(int row, int col)
        {
            this.row = row;
            this.col = col;
        }

        public int Row
        {
            get { return row; }
        }

        public int Col
        {
            get { return col; }
        }
    }
}
