﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Miscellaneous.Algorithms
{
    public class RecursiveStaircase
    {
        public int CountPathsRecurse(int steps)
        {
            if (steps < 0) return 0;
            else if (steps == 0) return 1;

            return CountPathsRecurse(steps - 1) + CountPathsRecurse(steps - 2) + CountPathsRecurse(steps - 3);
        }

        public int CountPathsMemo(int steps)
        {
            return CountPathsMemo(steps, new int[steps + 1]);
        }

        public int CountPathsMemo(int steps, int[] memo)
        {
            if (steps < 0) return 0;
            else if (steps == 0) return 1;

            if(memo[steps] == 0)
            {
                memo[steps] = CountPathsMemo(steps - 1, memo) + CountPathsMemo(steps - 2, memo) + CountPathsMemo(steps - 3, memo);
            }

            return memo[steps];
        }

        public int CountPathsDP(int steps)
        {
            if (steps < 0) return 0;
            else if (steps == 0) return 1;

            int[] paths = new int[steps + 1];
            paths[0] = 1;
            paths[1] = 1;
            paths[2] = 2;
            for(int i = 3; i <= steps; i++)
            {
                paths[i] = paths[i - 1] + paths[i - 2] + paths[i - 3];
            }

            return paths[steps];
        }
    }
}
