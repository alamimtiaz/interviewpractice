﻿using System;

namespace Miscellaneous.Algorithms
{
    public class PrintAllPairsSumToNumber
    {
        public void PrintAllPairs(int[] sorted, int M)
        {
            int left = 0;
            int right = sorted.Length - 1;

            while(left < right)
            {
                int sum = sorted[left] + sorted[right];

                if (sum == M)
                {
                    Console.WriteLine(string.Format("Sum({0},{1}={2}."), sorted[left], sorted[right], sum);
                    left++;
                    right--;
                }
                else if (sum > M) // sum is bigger than target, move the right cursor on left
                {
                    right--;
                }
                else // sum is less than target, move the left cursor on right
                {
                    left++;
                }
            }
        }
    }
}
