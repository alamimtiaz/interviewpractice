﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Miscellaneous.Algorithms
{
    // Boyer Moore Voting algorithm
    public class MajorityElement
    {
        public int Solve(int[] a)
        {
            int count = 0;
            int candidate = 0;

            for (int i = 0; i < a.Length - 1; i++)
            {
                if (count == 0)
                {
                    candidate = a[i];
                    count = 1;
                }
                else
                {
                    if (candidate == a[i])
                        count++;
                    else
                        count--;
                }
            }

            count = 0;
            for (int i = 0; i < a.Length - 1; i++)
            {
                if (a[i] == candidate)
                    count++;
            }

            if (count *2  >= a.Length - 1)
                return candidate;

            return -1;
        }
    }
}
