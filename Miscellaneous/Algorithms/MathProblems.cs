﻿using System;
using System.Collections.Generic;

namespace Miscellaneous.Algorithms
{
    public class MathProblems
    {
        public int FibRecurse(int n)
        {
            if (n == 0) return 0;
            if (n == 1) return 1;

            return FibRecurse(n - 1) + FibRecurse(n - 2);
        }

        public int Fib(int n, int[] memo)
        {
            if (n == 0) return 0;
            if (n == 1) return 1;

            if (memo[n] == 0)
                memo[n] = Fib(n - 1, memo) + Fib(n - 2, memo);

            return memo[n];
        }

        public int FibIterative(int n)
        {
            if (n == 0) return 0;
            if (n == 1) return 1;

            int result = 0;
            int prevResult = 1;
            int prevprevResult = 0;
            for(int i = 1; i < n; i++)
            {
                result = prevResult + prevprevResult;
                prevprevResult = prevResult;
                prevResult = result;
            }

            return result;
        }

        public double SqrtWithIteration(double a)
        {
            if (a < 0) return -1;
            if (a == 0 || a == 1) return a;

            double precision = 0.0001;
            double start = 0;
            double end = a;

            if (a < 1) end = 1;
            while((end-start) > precision)
            {
                double mid = (start + end) / 2;
                double midSqr = mid * mid;
                if (a == midSqr) return mid;
                else if (midSqr < a) start = mid;
                else end = mid;
            }

            return (start + end) / 2;
        }

        public int GreatestCommonDivisor(int a, int b)
        {
            int reminder = a % b;
            if (reminder == 0)
                return b;
            else
                return GreatestCommonDivisor(b, reminder);
        }

        public long MakeChange(int[] coins, int money)
        {
            return MakeChange(coins, money, 0);
        }

        private long MakeChange(int[] coins, int money, int index)
        {
            if (money == 0) return 1;
            if (index >= coins.Length) return 0;

            int amountWithCoin = 0;
            long ways = 0;
            while(amountWithCoin <= money)
            {
                int remaining = money - amountWithCoin;
                ways += MakeChange(coins, remaining, index + 1);
                amountWithCoin += coins[index];
            }

            return ways;
        }

        public long MakeChangeMemo(int[] coins, int money)
        {
            return MakeChangeMemo(coins, money, 0, new Dictionary<string, long>());
        }

        private long MakeChangeMemo(int[] coins, int money, int index, Dictionary<string, long> memo)
        {
            if (money == 0) return 1;
            if (index >= coins.Length) return 0;

            string key = money + "-" + index;
            if (memo.ContainsKey(key))
                return memo[key];

            int amountWithCoin = 0;
            long ways = 0;
            while (amountWithCoin <= money)
            {
                int remaining = money - amountWithCoin;
                ways += MakeChangeMemo(coins, remaining, index + 1, memo);
                amountWithCoin += coins[index];
            }

            memo[key] = ways;
            return ways;
        }

        public bool isPrime(int number)
        {
            if (number % 2 == 0) return number == 2;

            int sqrt = (int)Math.Sqrt(number);
            for (int i = 3; i <= sqrt; i = i + 2)
            {
                if (number % i == 0) return false;
            }

            return number != 1;
        }
    }
}
