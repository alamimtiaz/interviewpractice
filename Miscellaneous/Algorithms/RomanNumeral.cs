﻿using System;
using System.Text;

namespace Miscellaneous.Algorithms
{
    public class RomanNumeral
    {
        private static string[] numerals = new string[] { "M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I" };
        private static int[] values = new int[] { 1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1 };

        public string GetRomanNumeral(int number)
        {
            if (number < 1) throw new ArgumentException("Number cannot be more than 3999 or less than 1.");

            StringBuilder sb = new StringBuilder();
            int i = 0;
            while (number > 0)
            {
                if((number - values[i]) >= 0)
                {
                    number -= values[i];
                    sb.Append(numerals[i]);
                    i = 0; // go back the initial index ...
                }
                else
                {
                    i++;
                }
            }

            return sb.ToString();
        }
    }
}
