﻿using System;


namespace Miscellaneous.Algorithms
{
    // Given a string, find its rank among all its permutations sorted lexicographically. 
    // For example, rank of “abc” is 1, rank of “acb” is 2, and rank of “cba” is 6. 
    //
    // lexicographical order = Dictionary order
    // so if acb is given
    // and you find all permutations and sort (acb) will be 2nd order.
    public class LexicographicStringSort
    {
        public int Solve(String s)
        {
            return FindRank(s);
        }

        // A function to find rank of a string in all permutations
        // of characters
        private int FindRank(string str)
        {
            int len = str.Length;
            int mul = Factorial(len);
            int rank = 1;
            int countRight;

            int i;
            for (i = 0; i < len; ++i)
            {
                mul /= len - i;

                // count number of chars smaller than str[i]
                // fron str[i+1] to str[len-1]
                countRight = FindSmallerInRight(str, i, len - 1);

                rank += countRight * mul;
            }

            return rank;
        }

        // Factorial method
        private int Factorial(int n)
        {
            return (n <= 1) ? 1 : n * Factorial(n - 1);
        }

        // A utility function to count smaller characters on right
        // of arr[low]
        private int FindSmallerInRight(string str, int low, int high)
        {
            int countRight = 0, i;

            for (i = low + 1; i <= high; ++i)
                if (str[i] < str[low])
                    ++countRight;

            return countRight;
        }
    }
}