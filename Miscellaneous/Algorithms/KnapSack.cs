﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Miscellaneous
{
    public class KnapSack
    {
        /// <summary>
        /// 
        ///         /// </summary>
        /// <param name="w">Weights (stored in array w)</param>
        /// <param name="v">Values (stored in array v)</param>
        /// <param name="n">Number of distinct items</param>
        /// <param name="c">Knapsack capacity </param> 
        /// <returns></returns>
        public int s(int[] w, int[] v, int n, int c)
        {
            int[,] m = new int[c + 1, c + 1];

            for (int i = 1; i < n; i++)
            {
                for (int j = 0; j < c; j++)
                {
                    if (w[i] > j)
                    {
                        m[i, j] = m[i - 1, j];
                    }
                    else
                    {
                        m[i, j] = Math.Max(m[i - 1, j], m[i - 1, j - w[i]] + v[i]);
                    }
                }

                // max(m[i - 1, j], m[i - 1, j - w[i]] + v[i])

            }
            return 0;
        }
        // Put item in Knapsack which can only fit in Knapsack
        //
        // e.g. Weight: 1, 2, 4, 2, 5
        //       Value: 5, 3, 5, 3, 2

        public int GetZeroOneKnapSack(int[] Weight, int[] Value, int KnapSackCapacity)
        {
            int[,] dpArray = new int[Weight.Length + 1, KnapSackCapacity + 1];


            for (int itemIndex = 1; itemIndex <= Value.Length; itemIndex++)
            {
                for (int weightIndex = 0; weightIndex <= KnapSackCapacity; weightIndex++)
                {
                    if (itemIndex == 0 || weightIndex == 0)
                    {
                        dpArray[itemIndex, weightIndex] = 0;
                    }
                    else if (Weight[itemIndex - 1] > weightIndex)
                    {
                        dpArray[itemIndex, weightIndex] = dpArray[itemIndex - 1, weightIndex];
                    }
                    else
                    {
                        dpArray[itemIndex, weightIndex] = Math.Max(dpArray[itemIndex - 1, weightIndex],
                                                                  dpArray[itemIndex - 1, weightIndex - Weight[itemIndex - 1]] + Value[itemIndex - 1]);
                    }

                }

            }
            return dpArray[Weight.Length, KnapSackCapacity];
        }

    }
}