﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sort
{
    public class MergeSort
    {
        public int[] mergeSort(int[] list)
        {
            if (list.Length <= 1) return list;
            int midPoint = list.Length / 2;
            int[] left = new int[midPoint];
            int[] right = list.Length % 2 == 0 ? new int[midPoint] : new int[midPoint + 1];
            int[] result = new int[list.Length];
            for(int i = 0; i < midPoint; i++)
            {
                left[i] = list[i];
            }

            int index = 0;
            for (int i = midPoint; i < list.Length; i++)
            {
                if (index < right.Length)
                {
                    right[index] = list[i];
                    index++;
                }
            }
            left = mergeSort(left);
            right = mergeSort(right);

            result = merge(left, right);

            return result;
        }

        private int[] merge(int[] left, int[] right)
        {
            int lengthResult = left.Length + right.Length;
            int[] result = new int[lengthResult];
            int indexL = 0;
            int indexR = 0;
            int indexResult = 0;

            while(indexL <left.Length || indexR < right.Length)
            {
                if(indexL < left.Length && indexL < right.Length)
                {
                    if(left[indexL] <= right[indexR])
                    {
                        result[indexResult] = left[indexL];
                        indexL++;
                        indexResult++;
                    }
                    else
                    {
                        result[indexResult] = right[indexR];
                        indexR++;
                        indexResult++;
                    }
                }
                else if(indexL > left.Length)
                {
                    result[indexResult] = left[indexL];
                    indexL++;
                    indexResult++;
                }
                else if (indexL > right.Length)
                {
                    result[indexResult] = right[indexR];
                    indexR++;
                    indexResult++;
                }
            }

            return result;
        }
    }
}
