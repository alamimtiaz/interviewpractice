﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sort.Algorithm
{
    class MergeSort2
    {
        public int[] mergeSort(int[] arr)
        {
            return mergesort(arr, 0, arr.Length - 1);
        }

        //Sorting in non decreasing order
        private static int[] mergesort(int[] arr, int i, int j)
        {
            // TODO Auto-generated method stub
            int mid = 0;

            if (i < j)
            {
                mid = (i + j) / 2;
                // Console.WriteLine("mid="+mid);
                mergesort(arr, i, mid);
                mergesort(arr, mid + 1, j);
                return merge(arr, i, mid, j);
            }

            return new int[0];
        }

        private static int[] merge(int[] arr, int i, int mid, int j)
        {

            int[] temp = new int[arr.Length];
            int l = i;
            int r = j;
            int m = mid + 1;
            int k = l;

            while (l <= mid && m <= r)
            {
                if (arr[l] <= arr[m])
                {
                    temp[k++] = arr[l++];
                }
                else
                {
                    temp[k++] = arr[m++];
                }
            }

            while (l <= mid)
                temp[k++] = arr[l++];

            while (m <= r)
                temp[k++] = arr[m++];
   
            return temp;
        }
    }
}