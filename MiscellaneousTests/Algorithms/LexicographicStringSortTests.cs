﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Miscellaneous.Algorithms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Miscellaneous.Algorithms.Tests
{
    [TestClass()]
    public class LexicographicStringSortTests
    {
        [TestMethod()]
        public void LexicographicStringSortTest()
        {
            LexicographicStringSort lxs = new LexicographicStringSort();
            int order = lxs.Solve("PATNA");
            Assert.AreEqual(78, order);
        }
    }
}