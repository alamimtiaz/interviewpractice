﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Miscellaneous.Algorithms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Miscellaneous.Algorithms.Tests
{
    [TestClass()]
    public class MajorityElementTests
    {
        [TestMethod()]
        public void FindMajorityElement()
        {
            int[] a = { 4, 7, 7, 7, 4, 4, 4, 9, 4, 3 };

            MajorityElement me = new MajorityElement();
            Assert.AreEqual(4, me.Solve(a));
        }
    }
}