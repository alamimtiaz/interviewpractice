﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Miscellaneous.Algorithms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Miscellaneous.Algorithms.Tests
{
    [TestClass()]
    public class FindCombinationsOfWellFormedBracketsTests
    {
        [TestMethod()]
        public void FindCombinationsOfWellFormedBracketsTest()
        {
            FindCombinationsOfWellFormedBrackets wf = new FindCombinationsOfWellFormedBrackets();
            var result = wf.Solve(3);
            Assert.AreEqual(5, result.Count);
            Assert.AreEqual("((()))", result[0]);
            Assert.AreEqual("(()())", result[1]);
            Assert.AreEqual("(())()", result[2]);
            Assert.AreEqual("()(())", result[3]);
            Assert.AreEqual("()()()", result[4]);
        }
    }
}