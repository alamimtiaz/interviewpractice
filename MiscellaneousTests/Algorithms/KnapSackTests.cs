﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Miscellaneous;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Miscellaneous.Tests
{
    [TestClass()]
    public class KnapSackTests
    {
        [TestMethod()]
        public void GetZeroOneKnapSackTest()
        {
            KnapSack ks = new KnapSack();
            int[] W = { 1, 2, 4, 2, 5 };
            int[] V = { 5, 3, 5, 3, 2, };
            int result = ks.GetZeroOneKnapSack(W, V, 10);
            Assert.AreEqual(16, result);
        }

        [TestMethod()]
        public void GetZeroOneKnapSackTest2()
        {
            KnapSack ks = new KnapSack();
            int[] W = { 5, 4, 6, 3 };
            int[] V = { 10,40,30,50 };
            int result = ks.GetZeroOneKnapSack(W, V, 10);
            Assert.AreEqual(90, result);
        }
    }
}