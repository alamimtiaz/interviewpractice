// <copyright file="MineSweeperTest.cs">Copyright ©  2017</copyright>
using System;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Miscellaneous.Algorithms;

namespace Miscellaneous.Algorithms.Tests
{
    /// <summary>This class contains parameterized unit tests for MineSweeper</summary>
    [PexClass(typeof(MineSweeper))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [TestClass]
    public partial class MineSweeperTest
    {
        /// <summary>Test stub for Solve(Int32[,])</summary>
        [PexMethod]
        internal void SolveTest([PexAssumeUnderTest]MineSweeper target, int[,] board)
        {
            target.Solve(board);
            // TODO: add assertions to method MineSweeperTest.SolveTest(MineSweeper, Int32[,])
        }
    }
}
