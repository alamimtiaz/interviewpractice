﻿using System.Collections.Generic;

namespace Djkstra
{
    public class Dijkstra
    {
        List<int> queue = new List<int>();
        public double[] dist
        {
            get; set;
        }

        //Find distance from 0 node
        public Dijkstra(double[,] G)
        {
            Initialize(0, G.GetLength(0));

            // While there is unvisited node
            while (queue.Count > 0)
            {
                // Pick the minimum distance vertex from the set of vertices not
                // yet processed. u is always equal to src in first iteration.
                int u = getNextVertex();

                // Update dist value of the adjacent vertices of the picked vertex
                for (int v = 0; v < G.GetLength(0); v++)
                {
                    if (G[u, v] > 0)
                    {
                        if (dist[v] > dist[u] + G[u, v])
                        {
                            dist[v] = dist[u] + G[u, v];
                        }
                    }
                }
            }
        }


        // A utility function to find the vertex with minimum distance value, from
        // the set of vertices not yet included in processed
        int getNextVertex()
        {
            var min = double.PositiveInfinity;
            int vertex = -1;

            // Node with the least distance will be selected first
            foreach (int val in queue)
            {
                if (dist[val] <= min)
                {
                    min = dist[val];
                    vertex = val;
                }
            }
            queue.Remove(vertex);
            return vertex;
        }
        
        public void Initialize(int s, int len)
        {
            dist = new double[len];

            for (int i = 0; i < len; i++)
            {
                // All distance is infinite
                dist[i] = double.PositiveInfinity; // // Unknown distance from source to v

                // Add all vertex in queue
                queue.Add(i); // All nodes initially in Q (unvisited nodes)
            }
            dist[0] = 0; // Distance from source to source

        }
    }
}
