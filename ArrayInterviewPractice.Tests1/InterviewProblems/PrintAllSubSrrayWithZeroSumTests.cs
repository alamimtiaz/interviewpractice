﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ArrayApplication.InterviewProblems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayApplication.InterviewProblems.Tests
{
    [TestClass()]
    public class PrintAllSubSrrayWithZeroSumTests
    {
        [TestMethod()]
        public void PrintAllSubSrrayWithZeroSum_NaiveTest()
        {
            int[] arr = { 4, 2, -3, -1, 0, 4 };
            PrintAllSubSrrayWithZeroSum pas = new PrintAllSubSrrayWithZeroSum();
            List<List<int>> result = pas.PrintAllSubSrrayWithZeroSum_Naive(arr);
            
        }
    }
}