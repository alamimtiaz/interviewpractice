﻿using System.Collections.Generic;

namespace CrackingCodingInterview.Chapter3
{
    // Solution of 3.2
    public class StackWithMinimum : Stack<int>
    {
        private Stack<int> minimumStack;

        public StackWithMinimum()
        {
            minimumStack = new Stack<int>();
        }

        public new void Push(int value)
        {
            if (value < Minimum())
                minimumStack.Push(value);

            base.Push(value);
        }

        public new int Pop()
        {
            int value = base.Pop();
            if (value == Minimum())
                minimumStack.Pop();

            return value;
        }

        public int Minimum()
        {
            if (minimumStack.Count == 0)
                return int.MaxValue;
            else
                return minimumStack.Peek();
        }
    }
}
