﻿using System.Collections.Generic;

namespace CrackingCodingInterview.Chapter3
{
    public class QueueWithStack
    {
        private Stack<int> newest;
        private Stack<int> oldest;

        public QueueWithStack()
        {
            newest = new Stack<int>();
            oldest = new Stack<int>();
        }

        public void Enqueue(int val)
        {
            newest.Push(val);
        }

        public int Dequeue()
        {
            ShiftStacks();
            return oldest.Pop();
        }

        public int Peek()
        {
            ShiftStacks();
            return oldest.Peek();
        }

        public int Size()
        {
            return newest.Count + oldest.Count;
        }

        private void ShiftStacks()
        {
            if (oldest.Count == 0)
            {
                while (newest.Count != 0)
                {
                    oldest.Push(newest.Pop());
                }
            }
        }
    }
}
