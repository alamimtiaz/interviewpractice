﻿using System.Collections.Generic;

namespace CrackingCodingInterview.Chapter3
{
    public class SortStack
    {
        public Stack<int> Sort(Stack<int> source)
        {
            Stack<int> result = new Stack<int>();

            while(source.Count != 0)
            {
                int tmp = source.Pop();
                while(result.Count != 0 && result.Peek() > tmp)
                {
                    source.Push(result.Pop());
                }
                result.Push(tmp);
            }

            return result;
        }
    }
}
