﻿using System;
using System.Collections.Generic;

namespace CrackingCodingInterview.Chapter3
{
    // Solution of 3.3
    public class SetOfStacks
    {
        private const int STACK_SIZE = 100;
        Stack<Stack<int>> stacks = new Stack<Stack<int>>();

        public void Push(int v)
        {
            Stack<int> last = GetLastStack();
            if (last != null && last.Count < STACK_SIZE)
                last.Push(v);
            else
            {
                Stack<int> stack = new Stack<int>(STACK_SIZE);
                stack.Push(v);
                stacks.Push(stack);
            }
        }

        public int Pop()
        {
            Stack<int> last = GetLastStack();
            if (last == null) throw new ArgumentException();
            int val = last.Pop();
            if (last.Count == 0) stacks.Pop();
            return val;
        }

        public Stack<int> GetLastStack()
        {
            if (stacks.Count == 0) return null;
            return stacks.Peek();
        }
    }
}
