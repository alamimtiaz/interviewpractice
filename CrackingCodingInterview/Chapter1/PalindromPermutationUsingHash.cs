﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrackingCodingInterview.Chapter1
{
    public class PalindromPermutationUsingHash
    {
        // Use a hash table to count how many times eachcharacter appears.
        // Then, we iterate through the hash table and ensure that no more 
        // than one character hasan odd count.
        // Takes O(N) time and O(1) : Fixed space of (65536)
        public Boolean Solve(String Phrase)
        {
            if (String.IsNullOrWhiteSpace(Phrase))
                return false;

            int[] table = BuildCharFrequencyTable(Phrase);
            return CheckMaxOneOdd(table);
        }


        // Count how many times each character appears. 
        // Support Unicode characters.
        // Ask interviewer, if ascii only then declare table to 256.
        int[] BuildCharFrequencyTable(String phrase)
        {
            int[] table = new int[65536];

            foreach(char c in phrase)
            {
                table[c]++;
            }
            return table;
        }
            // Check that no more than one character has an odd count.
            Boolean CheckMaxOneOdd(int[] table)
        {
            Boolean FoundOdd = false;
            foreach (int Count in table)
            {
                if (Count % 2 == 1)
                {
                    if (FoundOdd)
                    {
                        return false;
                    }

                    FoundOdd = true;
                }
            }

            return true;
        }
    }
}