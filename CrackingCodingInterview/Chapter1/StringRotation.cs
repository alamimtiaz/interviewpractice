﻿namespace CrackingCodingInterview.Chapter1
{
    public class StringRotation
    {
        public bool IsRotation(string s1, string s2)
        {
            int len = s1.Length;
            if(len == s2.Length && len > 0)
            {
                string s1s1 = s1 + s1;
                return s1s1.Contains(s2);
            }

            return false;
        }
    }
}
