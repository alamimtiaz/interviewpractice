﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrackingCodingInterview.Chapter1
{

    public class IsUniqueCharsBitVector
    {

        // We can reduce our space usage by a factor of eight by using a bit vector. We will assume, in the below code,
        // that the string only uses the lowercase letters a through z.This will allow us to use just a single int

        // Space compelxity: O(N)
        public Boolean Solve(String str)
        {
            if (String.IsNullOrWhiteSpace(str) || str.Length > 256)
            {
                return false;
            }

            int checker = 0;
            for (int i = 0; i < str.Length; i++)
            {
                int val = str[i];

                //If ASCII value is greater than 255 or already found this char in string
                if (val > 255 || ((checker & (1 << val)) > 0))
                {
                    return false;
                }

                checker |= (1 << val);
            }
            return false;
        }
    }
}
