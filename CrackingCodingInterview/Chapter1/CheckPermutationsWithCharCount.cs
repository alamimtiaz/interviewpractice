﻿using System;


namespace CrackingCodingInterview.Chapter1
{
    public class CheckPermutationsWithCharCount
    {
        // We can also use the definition of a permutation-two words with the same character counts-to implement
        // this algorithm.We simply iterate through this code, counting how many times each character appears.
        // Then, afterwards, we compare the two arrays.
        //
        // SUPPORTS: Unicode chars
        public bool Solve(string first, string second)
        {
            // Assumptions: If both are null or white space then it's same string
            if (String.IsNullOrWhiteSpace(first) && String.IsNullOrWhiteSpace(second))
            {
                return true;
            }

            // If either of them is null or whitespace then it's not permutations of same string
            if (String.IsNullOrWhiteSpace(first) || String.IsNullOrWhiteSpace(second))
            {
                return false;
            }

            // Length should match
            if (first.Length != second.Length)
            {
                return false;
            }

            int[] CharSet = new int[65536];
            // Inrement index counter
            for(int i=0; i < first.Length; i++)
            {
                CharSet[first[i]]++;
            }

            // Decrement index counter
            for (int i = 0; i < second.Length; i++)
            {
                CharSet[second[i]]--;

                // If count is less than 0, then it cannot be a permutation
                if (CharSet[second[i]] < 0)
                {
                    return false;
                }
            }
            return true;
        }
    }
}
