﻿
namespace CrackingCodingInterview.Chapter1.CrackingCoding
{
    public class DoesStringHasUniqueCharacter
    {
        public bool IsUnique(string msg)
        {
            bool[] char_set = new bool[26];
            char[] chars = msg.ToCharArray();

            for(int i = 0; i < chars.Length; i++)
            {
                int index = chars[i] - 'a';
                if (char_set[i])
                    return false;
                char_set[index] = true;
            }

            return true;
        }
    }
}
