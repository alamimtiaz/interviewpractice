﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrackingCodingInterview.Chapter1
{
    public class URLify
    {
        public String Solve(String s, int TrueLength)
        {
            if (String.IsNullOrWhiteSpace(s))
            {
                return String.Empty;
            }

            if (s.Length < TrueLength)
            {
                return String.Empty;
            }

            return UrlEncodeBytesToBytes(s.ToCharArray(), TrueLength);
        }

        private String UrlEncodeBytesToBytes(char[] bytes, int count)
        {
            int SpaceCount = 0;

            // count them first
            for (int i = 0; i < count; i++)
            {
                if (bytes[i] == ' ')
                    SpaceCount++;
            }

            // Nothing to Expand
            if(SpaceCount==0)
            {
                return bytes.ToString();
            }

            int index = (count + SpaceCount * 2);
            
            // Assumptions, string should have sufficient space to hold URLified string.
            // Else return Empty String.
            // Clarify with the interviewer
            if(bytes.Length != index)
            {
                return String.Empty;
            }

            for (int i = count - 1;  i >= 0; i--)
            {
                if(bytes[i] == ' ')
                {
                    bytes[index - 1] = '0';
                    bytes[index - 2] = '2';
                    bytes[index - 3] = '%';
                    index -= 3;
                }
                else
                {
                    bytes[--index] = bytes[i];
                }
            }

            return new string(bytes); 
        }
    }
}
