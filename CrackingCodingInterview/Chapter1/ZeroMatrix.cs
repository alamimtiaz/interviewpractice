﻿namespace CrackingCodingInterview.Chapter1
{
    // Solution of 1.8
    public class ZeroMatrix
    {
        public void SetZeros(int[,] matrix)
        {
            bool[] rows = new bool[matrix.GetLength(0)];
            bool[] cols = new bool[matrix.GetLength(1)];

            // identofy which rows and columns to be nullified
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for(int j = 0; j < matrix.GetLength(1); j++)
                {
                    if(matrix[i,j] == 0)
                    {
                        rows[i] = true;
                        cols[j] = true;
                    }
                }
            }

            for(int i = 0; i < rows.Length; i++)
            {
                if (rows[i])
                    NullifyRow(matrix, i);
            }

            for(int j = 0; j < cols.Length; j++)
            {
                if (cols[j])
                    NullifyColumn(matrix, j);
            }
        }

        void NullifyRow(int[,] matrix, int row)
        {
            for (int j = 0; j < matrix.GetLength(1); j++)
                matrix[row, j] = 0;
        }

        void NullifyColumn(int[,] matrix, int column)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
                matrix[i, column] = 0;
        }
    }
}
