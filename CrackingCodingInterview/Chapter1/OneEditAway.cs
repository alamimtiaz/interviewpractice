﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrackingCodingInterview.Chapter1.CrackingCoding
{
    // Cracking Code Interview 1.5 solution
    public class OneEditAway
    {
        public bool IsOneEditAway(string first, string second)
        {
            if (first.Length == second.Length)
                return OneEditReplace(first, second);
            else if (first.Length + 1 == second.Length)
                return OneEditInsert(first, second);
            else if (first.Length == second.Length + 1)
                return OneEditInsert(second, first);

            return false;
        }

        private bool OneEditReplace(string first, string second)
        {
            bool foundDifference = false;
            char[] firstChars = first.ToCharArray();
            char[] secondChars = second.ToCharArray();

            for(int i = 0; i < firstChars.Length; i++)
            {
                if(firstChars[i] != secondChars[i])
                {
                    if (foundDifference)
                        return false;
                    foundDifference = true;
                }
            }
            return true;
        }

        private bool OneEditInsert(string first, string second)
        {
            int firstIndex = 0;
            int secondIndex = 0;
            char[] firstChars = first.ToCharArray();
            char[] secondChars = second.ToCharArray();
            while(firstIndex < firstChars.Length && secondIndex < secondChars.Length)
            {
                if(firstChars[firstIndex] != secondChars[secondIndex])
                {
                    if (firstIndex != secondIndex)
                        return false;
                    secondIndex++;
                }
                else
                {
                    firstIndex++;
                    secondIndex++;
                }
            }

            return true;
        }
    }
}
