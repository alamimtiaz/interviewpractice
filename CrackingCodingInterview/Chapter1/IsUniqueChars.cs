﻿using System;

namespace CrackingCodingInterview.Chapter1
{
    public class IsUniqueChars
    {
        // create an array of boolean values, where the flag at index i indicates whether character
        // i in the alphabet is contained in the string. The second time you see this character you 
        //can immediately return false.
        // Assumptions: Only ascii chars are supoorted
        // Note: 
        // Time Complexity : O(N)
        // Space compelxity: O(N)
        public Boolean Solve(String str)
        {
            if (String.IsNullOrWhiteSpace(str) || str.Length > 256)
            {
                return false;
            }

            Boolean[] CharSet = new Boolean[256];
            for (int i = 0; i < str.Length; i++)
            {
                int val = str[i];

                //If ASCII value is greater than 255 or already found this char in string
                if (val > 255 || CharSet[val])
                {
                    return false;
                }

                CharSet[val] = true;
            }
            return false;
        }
    }
}
