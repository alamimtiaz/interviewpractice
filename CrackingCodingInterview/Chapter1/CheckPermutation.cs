﻿using System; 

namespace CrackingCodingInterview.Chapter1.CrackingCoding
{
    public class CheckPermutation
    {
        // If two strings are permutations, then we know they have the same characters, but in different orders. Therefore,
        // sorting the strings will put the characters from two permutations in the same order.We just need to
        // compare the sorted versions of the strings.
        public bool Solve(string first, string second)
        { 
            // Assumptions: If both are null or white space then it's same string
            if(String.IsNullOrWhiteSpace(first) && String.IsNullOrWhiteSpace(second))
            {
                return true;
            }

            // If either of them is null or whitespace then it's not permutations of same string
            if(String.IsNullOrWhiteSpace(first) || String.IsNullOrWhiteSpace(second))
            {
                return false;
            }

            // Length should match
            if (first.Length != second.Length)
            {
                return false;
            }

            string firstSorted = Sort(first);
            string secondSorted = Sort(second);
            if (firstSorted.Equals(secondSorted))
                return true;
            return false;
        }

        private string Sort(string msg)
        {
            char[] chars = msg.ToCharArray();
            Array.Sort(chars);
            return new String(chars);
        }
    }
}
