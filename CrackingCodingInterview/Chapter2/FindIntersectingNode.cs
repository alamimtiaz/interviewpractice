﻿using CrackingCodingInterview.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrackingCodingInterview.Chapter2
{
    public class FindIntersectingNode
    {
        public Node FindIntersection(Node list1, Node list2)
        {
            if (list1 == null || list2 == null) return null;

            Result result1 = GetTailAndSize(list1);
            Result result2 = GetTailAndSize(list2);

            if (result1.Tail != result2.Tail) return null;

            Node shorter = result1.Size < result2.Size ? list1 : list2;
            Node longer = result1.Size > result2.Size ? list1 : list2;

            longer = GetKthNode(longer, Math.Abs(result1.Size - result2.Size));

            while(shorter != longer)
            {
                shorter = shorter.Next;
                longer = longer.Next;
            }

            return longer;
        }

        private Node GetKthNode(Node list, int k)
        {
            Node current = list;
            while(k > 0 && current != null)
            {
                current = current.Next;
                k--;
            }

            return current;
        }

        private Result GetTailAndSize(Node list)
        {
            if (list == null)
                return new Result(null, 0);

            int size = 1;
            Node current = list;
            while(current.Next != null)
            {
                current = current.Next;
                size++;
            }

            return new Result(current, size);
        }
    }

    public class Result
    {
        public Node Tail { get; set; }
        public int Size { get; set; }
        public Result(Node tail, int size)
        {
            Tail = tail;
            Size = size;
        }
    }
}
