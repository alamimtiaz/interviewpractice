﻿using System.Collections.Generic;

using CrackingCodingInterview.Model;

namespace CrackingCodingInterview.Chapter2
{
    // Solution of 2.1
    public class RemoveDuplicates
    {
        public void Solve(Node node)
        {
            HashSet<int> hash = new HashSet<int>();
            Node prev = null;
            while(node != null)
            {
                if(hash.Contains(node.Data))
                {
                    prev.Next = node.Next;
                }
                else
                {
                    hash.Add(node.Data);
                    prev = node;
                }
                node = node.Next;
            }
        }
    }
}
