﻿using CrackingCodingInterview.Model;

namespace CrackingCodingInterview.Chapter2
{
    public class ReturnKthItemToLast
    {
        // Solution of 2.2
        public Node NthItemToLast(Node head, int k)
        {
            Node p1 = head;
            Node p2 = head;

            for(int i = 0; i < k; i++)
            {
                if (p1 == null) return null;
                p1 = p1.Next;
            }

            while(p1 != null)
            {
                p1 = p1.Next;
                p2 = p2.Next;
            }

            return p2;
        }
    }
}
