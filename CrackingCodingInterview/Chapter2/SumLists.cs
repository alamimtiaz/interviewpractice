﻿using CrackingCodingInterview.Model;

namespace CrackingCodingInterview.Chapter2
{
    public class SumLists
    {
        public Node AddList(Node l1, Node l2, int carry)
        {
            if(l1 == null && l2 == null && carry == 0)
            {
                return null;
            }

            Node result = new Node();
            int val = carry;
            if (l1 != null)
                val += l1.Data;
            if (l2 != null)
                val += l2.Data;

            result.Data = val % 10;

            if(l1 != null || l2 != null)
            {
                Node more = AddList(l1 == null ? null : l1.Next, l2 == null ? null : l2.Next, val >= 10 ? 1 : 0);
                result.Next = more;
            }

            return result;
        }
    }
}
