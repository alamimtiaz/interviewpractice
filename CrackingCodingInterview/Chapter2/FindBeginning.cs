﻿using CrackingCodingInterview.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrackingCodingInterview.Chapter2
{
    // Solution of 2.8
    public class FindBeginning
    {
        public Node FindBeginningNode(Node head)
        {
            Node slow = head;
            Node fast = head;

            while(fast != null && fast.Next != null)
            {
                slow = slow.Next;
                fast = fast.Next.Next;

                if (slow == fast)
                    break;
            }

            if (fast == null || fast.Next == null)
                return null;

            slow = head;
            while(slow != fast)
            {
                slow = slow.Next;
                fast = fast.Next;
            }

            return fast;
        }
    }
}
