﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CrackingCodingInterview.Chapter1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrackingCodingInterview.Chapter1.Tests
{
    [TestClass()]
    public class RotateMatrixTests
    {
        [TestMethod()]
        public void RotateTest()
        {
            int[,] matrix =  { { 1,2,3,4},
                               { 5,6,7,8},
                               { 9,10,11,12},
                               { 13,14,15,16}
            };

            int[,] expected =  { { 13,9,5,1},
                               { 5,10,7,2},
                               { 9,10,11,3},
                               { 13,14,15,4}
            };
            RotateMatrix rm = new RotateMatrix();
            var result = rm.Rotate(matrix,4);

           

            Assert.AreEqual(1,1);
        }
    }
}