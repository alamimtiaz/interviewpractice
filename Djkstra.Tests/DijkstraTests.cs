﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Djkstra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Djkstra.Tests
{
    [TestClass()]
    public class DijkstraTests
    {
        [TestMethod()]
        public void DijkstraTest1()
        {
            double[,] G = new double[4, 4];
            G[0, 1] = 3;
            G[0, 3] = 6;
            G[1, 3] = 1;
            G[1, 2] = 3;
            G[3, 2] = 1;
            Dijkstra dijkstra = new Dijkstra(G);
            Assert.AreEqual(3.0, dijkstra.dist[1]);
            Assert.AreEqual(5.0, dijkstra.dist[2]);
            Assert.AreEqual(4.0, dijkstra.dist[3]);
        }

        [TestMethod()]
        public void DijkstraTest2()
        {
            double[,] G = new double[5, 5];
            G[0, 1] = 4;
            G[0, 3] = 8;
            G[1, 2] = 3;
            G[2, 3] = 4;
            G[3, 4] = 7;
            Dijkstra dijkstra = new Dijkstra(G);
            Assert.AreEqual(4, dijkstra.dist[1]);
            Assert.AreEqual(7, dijkstra.dist[2]);
            Assert.AreEqual(8, dijkstra.dist[3]);
            Assert.AreEqual(15, dijkstra.dist[4]);
        }
    }
}