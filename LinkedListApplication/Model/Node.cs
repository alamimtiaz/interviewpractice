﻿namespace LinkedListInterviewPractice.Model
{
    public class Node
    {
        private int data;
        public Node Next { get; set; }
        
        public Node(int data)
        {
            this.data = data;
        }

        public override string ToString()
        {
            return data.ToString();
        }

        public int Data
        {
            get
            {
                return data;
            }
        }

    }
}
