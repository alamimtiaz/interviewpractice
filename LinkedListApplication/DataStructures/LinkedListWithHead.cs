﻿using System;
using System.Collections.Generic;

using LinkedListInterviewPractice.Model;
using LinkedListInterviewPractice.InterviewProblems;

namespace LinkedListInterviewPractice.DataStructures
{
    public class LinkedListWithHead
    {
        public Node head;

        public LinkedListWithHead()
        {
            head = null;
        }

        public void AddFirst(int data)
        {
            Console.WriteLine("One item is added at the beginning.");
            Node newNode = new Node(data);
            newNode.Next = head;
            head = newNode;
        }

        public void AddLast(int data)
        {
            Console.WriteLine("One item is added at the end.");
            Node newNode = new Node(data);
            if(head == null)
            {
                head = newNode;
            }
            else
            {
                Node current = head;
                while(current.Next != null)
                {
                    current = current.Next;
                }
                current.Next = newNode;
            }
        }

        public void RemoveFirst()
        {
            if (head == null)
            {
                Console.WriteLine("No element exist in the linked list");
                return;
            }

            Console.WriteLine("One item is removed from the beginning.");
            head = head.Next;
        }

        public void RemoveLast()
        {
            if (head == null)
            {
                Console.WriteLine("No element exist in the linked list");
                return;
            }

            Node prev = null;
            Node current = head;
            while(current.Next != null)
            {
                prev = current;
                current = current.Next;
            }
            if (prev == null)
                head = null;
            else
                prev.Next = null;
        }

        public bool IsPalindrome()
        {
            Node current = head;
            Node runner = head;
            Stack<int> stack = new Stack<int>();

            while(runner != null || runner.Next != null)
            {
                stack.Push(current.Data);
                current = current.Next;
                runner = runner.Next.Next;
            }

            if (runner != null)
                current = current.Next;

            while(current != null)
            {
                if (stack.Pop() != current.Data) return false;
                current = current.Next;
            }

            return true;
        }

        public bool IsEmpty()
        {
            return head == null;
        }

        public int Count
        {
            get
            {
                Node current = head;
                int count = 0;
                while (current != null)
                {
                    current = current.Next;
                    count++;
                }

                return count;
            }
        }

        public void Print()
        {
            Node currentNode = head;
            while (currentNode != null)
            {
                Console.WriteLine(currentNode.ToString());
                currentNode = currentNode.Next;
            }
        }

        public void ReversePrint()
        {
            ReversePrint(head);
        }

        public void ReversePrint(Node node)
        {
            if (node == null) return;
            ReversePrint(node.Next);
            Console.WriteLine(node.Data);
        }

        public static Node Merge(Node node1, Node node2)
        {
            if (node1 == null) return node2;
            if (node2 == null) return node1;

            if(node1.Data < node2.Data)
            {
                return Concat(node1, Merge(node2, node1.Next));
            }
            return Concat(node2, Merge(node1, node2.Next));
        }

        private static Node Concat(Node node1, Node node2)
        {
            node1.Next = node2;
            return node1;
        }

        private Node FindNToLastNode(int n)
        {
            Node current = head;
            Node behind = head;

            for (int i = 0; i < n; i++)
            {
                // assumption the list has enough nodes
                if(current.Next != null) current = current.Next;
            }

            while(current.Next != null)
            {
                current = current.Next;
                behind = behind.Next;
            }

            return behind;
        }

        public void ReverseList()
        {
            Node prev = null;
            Node current = head;

            while (current != null)
            {
                Node temp = current.Next;
                current.Next = prev;
                prev = current;
                current = temp;
            }

            head = prev;
        }

        public void SwapTwoNodes()
        {
            SwapTwoNodes swap = new SwapTwoNodes();
            head = swap.Solve(head);
        }

        public void DeleteAlternateNodes()
        {
            DeleteAlternateNodes deleteNodes = new InterviewProblems.DeleteAlternateNodes();
            deleteNodes.Solve(head);
        }

        //public void ReverseList()
        //{
        //    head = ReverseList(head, null);
        //}
        //
        //private Node ReverseList(Node current, Node previous)
        //{
        //    if (current == null) return current;

        //    if(current.Next == null)
        //    {
        //        current.Next = previous;
        //        return current;
        //    }

        //    Node tmp = current.Next;
        //    current.Next = previous;
        //    return ReverseList(tmp, current);
        //}

        public bool HasCycle(Node head)
        {
            if (head == null) return false;
            Node fast = head.Next;
            Node slow = head;
            while(fast != null && slow != null)
            {
                if(fast == slow)
                {
                    return true;
                }
                fast = fast.Next.Next;
                slow = slow.Next;
            }
            return false;
        }
    }
}
