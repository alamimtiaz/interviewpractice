﻿using System;

using LinkedListInterviewPractice.Model;

namespace LinkedListInterviewPractice.DataStructures
{
    public class LinkedListWithRoot
    {
        Node root;
        int length;

        public LinkedListWithRoot()
        {
            root = null;
            length = 0;
        }

        public void insertLast(int data)
        {
            root = insertLastPrivate(root, data);
            length++;
        }

        private Node insertLastPrivate(Node root, int data)
        {
            if(root == null)
            {
                root = new Node(data);
            }
            else
            {
                root.Next = insertLastPrivate(root.Next, data);
            }

            return root;
        }

        public void insertFirst(int data)
        {
            Node newNode = new Node(data);
            newNode.Next = root;
            root = newNode;
        }

        public void removeFirst()
        {
            if(root == null)
            {
                Console.WriteLine("List is empty!");
            }
            else
            {
                root = root.Next;
            }
        }

        public void removeLast()
        {

        }

        public void Traverse()
        {
            TraversePrivate(root);
        }

        private void TraversePrivate(Node root)
        {
            if(root == null)
            {
                return;
            }
            else
            {
                TraversePrivate(root.Next);
                Console.WriteLine(root.Data);
            }
        }
    }
}
