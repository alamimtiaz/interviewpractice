﻿using System;

using LinkedListInterviewPractice.Model;

namespace LinkedListInterviewPractice.DataStructures
{
    public class LinkedListWithHeadAndTail
    {
        private Node head;
        private Node tail;

        public LinkedListWithHeadAndTail()
        {
            head = null;
            tail = null;
        }

        public void AddFirst(int data)
        {
            Node newNode = new Node(data);
            if (head == null)
            {
                head = newNode;
                tail = newNode;
            }
            else
            {
                newNode.Next = head;
                head = newNode;
            }
        }

        public void AddLast(int data)
        {
            Node newNode = new Node(data);
            if(tail == null)
            {
                head = newNode;
                tail = newNode;
            }
            else
            {
                tail.Next = newNode;
                tail = newNode;
            }
        }

        public void RemoveFirst()
        {
            if (head == null) return;
            if(head == tail)
            {
                head = null;
                tail = null;
            }
            else
            {
                head = head.Next;
            }
        }

        public void RemoveLast()
        {
            if (tail == null) return;
            if(head == tail)
            {
                head = null;
                tail = null;
            }
            else
            {
                Node prev = null;
                Node current = head;
                while(current.Next != null)
                {
                    prev = current;
                    current = current.Next;
                }
                tail = prev;
                prev.Next = null;
            }
        }

        public bool IsEmpty()
        {
            return head == null;
        }

        public void Print()
        {
            Node currentNode = head;
            Console.WriteLine("Start!");
            while (currentNode != null)
            {
                Console.WriteLine(currentNode.ToString());
                currentNode = currentNode.Next;
            }
            Console.WriteLine("End!");
        }
    }
}
