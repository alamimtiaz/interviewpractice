﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinkedListInterviewPractice.Model;

namespace LinkedListInterviewPractice.DataStructures
{
    public class CircularList
    {
        private CircularNode head;

        public void Insert(int n)
        {
            if (head == null) // when there is not item in the list
            {
                head = new CircularNode(n);
                head.Next = head;
            }
            else if(head.Next == head) // when there is only one item
            {
                head.Next = new CircularNode(n);
                head.Next.Next = head;
                head = head.Data > n ? head.Next : head; // move head to smallest node
            }
            else if(n < head.Data) // when there are multiple items and new value smaller than 
            {
                CircularNode current = head;
                while(current.Next != head) // find the tail
                    current = current.Next;

                current.Next = new CircularNode(n); // add after the tail
                current.Next.Next = head;
                head = current.Next;
            }
            else // otherwise, find the appropriate position and insert it
            {
                CircularNode current = head;
                while (current.Next != head && current.Next.Data < n) 
                    current = current.Next;

                CircularNode currentNext = current.Next;
                current.Next = new CircularNode(n); 
                current.Next.Next = currentNext;
            }
        }

        public void PrintList()
        {
            if (head == null) return;

            CircularNode current = head;
            do
            {
                Console.Write(current.Data+ ",");
                current = current.Next;
            } while (current != head);

            Console.WriteLine();
        }
    }

    public class CircularNode
    {
        public int Data;
        public CircularNode Next;

        public CircularNode(int k)
        {
            Data = k;
            Next = null;
        }
    }
}
