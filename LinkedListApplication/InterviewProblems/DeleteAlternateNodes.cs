﻿using LinkedListInterviewPractice.Model;

namespace LinkedListInterviewPractice.InterviewProblems
{
    public class DeleteAlternateNodes
    {
        public void Solve(Node head)
        {
            if (head == null || head.Next == null) return;

            Node current = head;

            while(current != null && current.Next != null)
            {
                current.Next = current.Next.Next; // skip the next node
                current = current.Next; // move to next node
            }
        }
    }
}
