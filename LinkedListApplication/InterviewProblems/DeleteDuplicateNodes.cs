﻿using LinkedListInterviewPractice.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinkedListApplication.InterviewProblems
{
    public class DeleteDuplicateNodes
    {
        // Using extra space (hash)
        public void Dedup(Node node)
        {
            if (node == null) return;

            HashSet<int> hash = new HashSet<int>();
            Node prev = null;
            Node current = node;
            while(current != null)
            {
                if(hash.Contains(current.Data))
                {
                    // current node is duplicate, remove
                    prev.Next = current.Next;
                }
                else
                {
                    // current node needs to be added to hash
                    hash.Add(current.Data);
                }
                // traverse
                prev = current;
                current = current.Next;
            }
        }

        public void Dedup2(Node node)
        {
            if (node == null) return;

            while(node != null)
            {
                Node current = node;
                while(current.Next != null)
                {
                    if(current.Next.Data == node.Data)
                    {
                        // delete duplicate node
                        current.Next = current.Next.Next;
                    }
                    else
                    {
                        current = current.Next;
                    }
                }
            }
        }
    }
}
