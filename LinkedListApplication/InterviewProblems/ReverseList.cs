﻿using LinkedListInterviewPractice.Model;

namespace LinkedListInterviewPractice.InterviewProblems
{
    public class ReverseList
    {
        public void Solve(Node head)
        {
            Node prev = null;
            Node current = head;

            while(current != null)
            {
                Node temp = current.Next;
                current.Next = prev;
                prev = current;
                current = temp;
            }

            head = prev;
        }
    }
}
