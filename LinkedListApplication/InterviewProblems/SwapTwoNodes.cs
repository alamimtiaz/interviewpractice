﻿using LinkedListInterviewPractice.Model;

namespace LinkedListInterviewPractice.InterviewProblems
{
    public class SwapTwoNodes
    {
        public Node Solve(Node head)
        {
            if (head == null || head.Next == null)
                return head;

            Node n = head.Next;
            head.Next = Solve(head.Next.Next);
            n.Next = head;
            return n;
        }

        public Node Solve2(Node head)
        {
            if (head == null || head.Next == null) return head;

            Node newHead = head.Next;
            Node current = head;
            Node prev = null; //extra lines
            Node tmp;

            while(current != null && current.Next != null)
            {
                tmp = current.Next;
                if(prev != null) prev.Next = tmp; //extra lines
                current.Next = current.Next.Next;
                tmp.Next = current;
                current = tmp;
                prev = current.Next; //extra lines
                current = current.Next.Next;
            }

            return newHead;
        }
    }
}
