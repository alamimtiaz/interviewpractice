﻿using System;

using LinkedListInterviewPractice.Model;

namespace LinkedListInterviewPractice.InterviewProblems
{
    public class FindMidPoint
    {
        int length = 0;
        int currentReverseIndex = 0;

        /// <summary>
        /// This method would find mid points with N time
        /// It does not require two scans
        /// </summary>
        /// <param name="node"></param>
        public void Solve(Node node)
        {
            if(node != null)
            {
                length++;
                Solve(node.Next);
                currentReverseIndex++;
            }

            if(currentReverseIndex*2 == length || (currentReverseIndex*2 == length + 1))
            {
                Console.WriteLine("Midpoint: " + node.Data);
            }
        }
    }
}
