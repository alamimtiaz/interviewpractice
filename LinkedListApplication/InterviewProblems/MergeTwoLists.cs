﻿using LinkedListInterviewPractice.Model;

namespace LinkedListApplication.InterviewProblems
{
    public class MergeTwoLists
    {
        public Node Merge(Node l1, Node l2)
        {
            if (l1 == null) return l2;
            if (l2 == null) return l1;
            if (l1.Data < l2.Data)
            {
                l1.Next = Merge(l1.Next, l2);
                return l1;
            }
            else
            {
                l2.Next = Merge(l1, l2.Next);
                return l2;
            }
        }
    }
}
