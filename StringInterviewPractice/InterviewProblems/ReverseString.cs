﻿using System;

namespace StringInterviewPractice.InterviewProblems
{
    public class ReverseString
    {
        public string Solve(string message)
        {
            char[] chars = message.ToCharArray();
            Reverse(chars, 0, chars.Length - 1);

            return new String(chars);
        }

        private void Reverse(char[] chars, int start, int end)
        {
            if (start >= end) return;

            for (int i = start; i < end / 2; i++)
            {
                char temp = chars[i];
                chars[i] = chars[end - i];
                chars[end - i] = temp;
            }
        }
    }
}
