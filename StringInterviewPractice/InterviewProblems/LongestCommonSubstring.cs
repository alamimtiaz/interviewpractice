﻿namespace StringInterviewPractice.InterviewProblems
{
    public class LongestCommonSubstring
    {
        public string GetLongestCommonSubstring(string a, string b)
        {
            if (a.Length == 0 || b.Length == 0) return string.Empty;

            string output = string.Empty;
            int[,] cache = new int[a.Length, b.Length];

            char[] achars = a.ToLower().ToCharArray();
            char[] bchars = b.ToLower().ToCharArray();

            for (int i = 0; i < achars.Length; i++)
            {
                for (int j = 0; j < bchars.Length; j++)
                {
                    if (achars[i] == bchars[j])
                    {
                        if (i == 0 || j == 0)
                        {
                            cache[i, j] = 1;
                        }
                        else
                        {
                            cache[i, j] = cache[i - 1, j - 1] + 1;
                        }
                        if (cache[i, j] > output.Length)
                        {
                            int index = i - cache[i, j] + 1;
                            output = a.Substring(index, cache[i, j]); // start index = i - cache[i,j]
                        }

                    }
                }
            }


            return output;
        }
    }
}