﻿using System;

namespace StringInterviewPractice.InterviewProblems
{
    public class FindPalindrome
    {
        public bool Solve(string msg)
        {
            char[] chars = msg.ToCharArray();
            Array.Reverse(chars);
            String reverseStr = new String(chars);
            if (msg.Equals(reverseStr))
                return true;

            return false;
        }
    }
}
