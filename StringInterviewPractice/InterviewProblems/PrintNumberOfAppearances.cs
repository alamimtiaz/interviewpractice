﻿using System;
using System.Text;

namespace StringInterviewPractice.InterviewProblems
{
    public class PrintNumberOfAppearances
    {
        // You have a string aaabbbdcccccf, transform it the following way: a3b3d1c5f1
        // ie. aabbaa -> a2b2a2
        // TODO: Can it be improved
        public void Solve(string msg)
        {
            char[] chars = msg.ToCharArray();
            StringBuilder sb = new StringBuilder();

            int count = 0;
            char prev = '\0';
            for (int i = 0; i < chars.Length; i++)
            {
                char ch = chars[i];

                if (i == 0)
                {
                    sb.Append(chars[i]);
                }
                else if (chars[i] != prev)
                {
                    sb.Append(count);
                    sb.Append(chars[i]);
                    count = 0;
                }
                else if (i == chars.Length - 1)
                {
                    count++;
                    sb.Append(count);
                }

                count++;
                prev = ch;
            }

            Console.WriteLine(sb.ToString());
        }
    }
}
