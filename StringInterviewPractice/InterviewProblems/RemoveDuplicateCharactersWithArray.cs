﻿using System.Text;

namespace StringInterviewPractice.InterviewProblems
{
    public class RemoveDuplicateCharactersWithArray
    {
        public string Solve(string msg)
        {
            int NUMBER_CHARACTERS = 26;
            StringBuilder sb = new StringBuilder();
            char[] chars = msg.ToLower().ToCharArray();
            bool[] duplicateTable = new bool[NUMBER_CHARACTERS];

            foreach (char ch in chars)
            {
                int index = ch - 'a';
                if (!duplicateTable[index]) // check if the entry exists
                {
                    // put the existance flag to hash table and
                    // add the char to result string
                    duplicateTable[index] = true;
                    sb.Append(ch);
                }
            }

            return sb.ToString();
        }
    }
}
