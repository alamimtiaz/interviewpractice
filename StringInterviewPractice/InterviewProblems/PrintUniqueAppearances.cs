﻿using System;
using System.Collections.Generic;

namespace StringInterviewPractice.InterviewProblems
{
    public class PrintUniqueAppearances
    {
        // Given a string, print out all of the unique characters 
        // and the number of times it appeared in the string
        public void Solve(string msg)
        {
            char[] chars = msg.ToLower().ToCharArray();
            Dictionary<char, int> charCounts = new Dictionary<char, int>();

            for (int i = 0; i < chars.Length; i++)
            {
                if (!charCounts.ContainsKey(chars[i]))
                    charCounts.Add(chars[i], 1);
                else
                    charCounts[chars[i]] = charCounts[chars[i]] + 1;
            }

            foreach (var item in charCounts)
            {
                Console.WriteLine(string.Format("char {0}: {1}.", item.Key, item.Value));
                Console.WriteLine("----------");
            }
        }
    }
}
