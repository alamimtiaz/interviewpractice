﻿using System.Text;

namespace StringInterviewPractice.InterviewProblems
{
    public class ReverseWords
    {
        public string Solve(string message)
        {
            string[] words = message.Split(' ');

            StringBuilder sb = new StringBuilder();
            for (int i = words.Length - 1; i >= 0; i--)
            {
                sb.Append(words[i]);
                sb.Append(" ");
            }

            return sb.ToString();
        }
    }
}
