﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringInterviewPractice.InterviewProblems
{
    public class RabinKarp
    {
        private int prime = 31;

        public int PatternSearch(String text, string pattern)
        {
            long patternHash = GetHashCode(pattern, pattern.Length - 1);
            long textHash = GetHashCode(text, pattern.Length - 1);

            int m = pattern.Length;
            int n = text.Length;

            for (int i = 1; i <= n - m + 1; i++)
            {
                if (patternHash == textHash && CheckEqual(text, i - 1, i + m - 2, pattern, 0, m - 1))
                {
                    return i - 1;
                }
                if (i < n - m + 1)
                {
                    textHash = recalculateHash(text, i - 1, i + m - 1, textHash, m);
                }
            }
            return -1;
        }

        private long recalculateHash(String str, int oldIndex, int newIndex, long oldHash, int patternLen)
        {
            long newHash = oldHash - str[oldIndex];
            newHash = newHash / prime;
            newHash += str[newIndex] * (long)Math.Pow(prime, patternLen - 1);
            return newHash;
        }


        private long GetHashCode(String str, int length)
        {
            long hash = 0;
            for (int i = 0; i <= length; i++)
            {
                hash += str[i] * (long)Math.Pow(prime, i);
            }
            return hash;
        }

        private bool CheckEqual(String str1, int start1, int end1, String str2, int start2, int end2)
        {
            if (end1 - start1 != end2 - start2)
            {
                return false;
            }
            while (start1 <= end1 && start2 <= end2)
            {
                if (str1[start1] != str2[start2])
                {
                    return false;
                }
                start1++;
                start2++;
            }
            return true;
        }
    }
}
