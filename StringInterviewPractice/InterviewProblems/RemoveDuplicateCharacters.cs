﻿using System.Collections.Generic;
using System.Text;

namespace StringInterviewPractice.InterviewProblems
{
    public class RemoveDuplicateCharacters
    {
        public string Solve(string msg)
        {
            StringBuilder sb = new StringBuilder();
            char[] chars = msg.ToLower().ToCharArray();
            Dictionary<char, bool> duplicateTable = new Dictionary<char, bool>();

            foreach (char ch in chars)
            {
                if (!duplicateTable.ContainsKey(ch)) // check if the entry exists
                {
                    // put the existance flag to hash table and
                    // add the char to result string
                    duplicateTable.Add(ch, true);
                    sb.Append(ch);
                }
            }

            return sb.ToString();
        }
    }
}
