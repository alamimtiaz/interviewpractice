﻿
namespace StringInterviewPractice.InterviewProblems
{
    public class FindFirstDuplicateCharacter
    {
        public char Solve(string msg)
        {
            int NUMBER_CHARACTERS = 26;
            char[] chars = msg.ToLower().ToCharArray();
            bool[] duplicateTable = new bool[NUMBER_CHARACTERS];

            foreach (char ch in chars)
            {
                int index = ch - 'a';
                if (duplicateTable[index]) // IsDuplicate
                {
                    return ch;
                }
                else
                {
                    // update duplicate table
                    duplicateTable[index] = true;
                }
            }

            return '\0';
        }
    }
}
