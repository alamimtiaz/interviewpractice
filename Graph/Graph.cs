﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graph
{
    class Graph
    {
        List<int>[] adj;
        int V;

        Graph(int v)
        {
            this.V = v;
            adj = new List<int>[v];
        }

        void AddEdge(int u, int v)
        {
            adj[u] = new List<int>();
            adj[v] = new List<int>();

            adj[u].Add(v);
            adj[v].Add(u);
        }

        int CountEdges()
        {
            int sum = 0;

            //traverse all vertex
            for (int i = 0; i < V; i++)

                // add all edge that are linked to the
                // current vertex
                sum += adj[i].Count();


            // The count of edge is always even because in
            // undirected graph every edge is connected
            // twice between two vertices
            return sum / 2;

        }
    }
}
