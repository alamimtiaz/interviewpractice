﻿using System.Collections.Generic;

using GraphInterviewPractice.Model;

namespace GraphInterviewPractice.Algorithms
{
    public class TopologicalSort
    {
        public Stack<Node> TopSort(Graph graph)
        {
            Stack<Node> stack = new Stack<Node>();
            HashSet<Node> visited = new HashSet<Node>();

            foreach(Node node in graph.GetAllNodes())
            {
                if (visited.Contains(node)) continue;
                TopSortHelper(node, stack, visited);
            }

            return stack;
        }

        private void TopSortHelper(Node node, Stack<Node> stack, HashSet<Node> visited)
        {
            visited.Add(node);
            foreach(Node child in node.AdjacentNodes)
            {
                if (visited.Contains(child)) continue;
                TopSortHelper(child, stack, visited);
            }
            stack.Push(node);
        }
    }
}
