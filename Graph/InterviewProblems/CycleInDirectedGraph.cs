﻿using GraphInterviewPractice.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphInterviewPractice.InterviewProblems
{
    // Source: https://www.youtube.com/watch?v=rKQaZuoUR4M
    public class CycleInDirectedGraph
    {
        public bool HasCycle(Graph graph)
        {
            HashSet<Node> whiteSet = new HashSet<Node>();
            HashSet<Node> greySet = new HashSet<Node>();
            HashSet<Node> blackSet = new HashSet<Node>();

            foreach(Node node in graph.GetAllNodes())
            {
                whiteSet.Add(node);
            }

            while(whiteSet.Count != 0)
            {
                Node current = whiteSet.First();
                if(dfs(current, whiteSet, greySet, blackSet))
                {
                    return true;
                }
            }

            return false;
        }

        private bool dfs(Node current, HashSet<Node> whiteSet, HashSet<Node> greySet, HashSet<Node> blackSet)
        {
            moveNode(current, whiteSet, greySet);

            foreach(Node neighbor in current.AdjacentNodes)
            {
                if (blackSet.Contains(neighbor)) continue;
                if (greySet.Contains(neighbor)) return true;
                if (dfs(neighbor, whiteSet, greySet, blackSet))
                    return true;
            }

            moveNode(current, greySet, whiteSet);
            return false;
        }

        private void moveNode(Node node, HashSet<Node> source, HashSet<Node> target)
        {
            source.Remove(node);
            target.Add(node);
        }
    }
}
