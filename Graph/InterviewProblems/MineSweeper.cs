﻿using System.Collections.Generic;


namespace GraphInterviewPractice.InterviewProblems
{

    // Minesweeper question: Given an n x m grid holding individual mine co-ordinates, identify all of the mine clusters. 
    //A mine cluster is the largest collection of neighboring mines.


    // Minesweeper question: Given an n x m grid holding individual mine co-ordinates, identify all of the mine clusters.
    // A mine cluster is the largest collection of neighboring mines.
    //
    // Example input:
    //  0 1 2 3
    // ---------
    // 0|0|1|0|1
    // 1|0|0|1|1
    // 2|0|0|0|0
    // 3|1|1|0|0
    //
    //Expected output:
    //
    //[cluster 1] (0,1),(1,2),(1,3),(0,3)
    //[cluster 2] (3,0),(3,1)</p>


    public class MineSweeper
    {
        public class Result
        {
            public int x { get; set; }
            public int y { get; set; }

            public Result(int x, int y)
            {
                this.x = x;
                this.y = y;
            }
        }

        List<Result> list;
        // Assumes all positive integers
        public IList<Result> Solve(int[,] board)
        {
            if (board == null || board.GetLength(0) <2 || board.GetLength(1) < 2)
                return null;

            list = new List<Result>();
            bool[,] visited = new bool[board.GetLength(1), board.GetLength(0)];
            for (int i = 0; i < board.GetLength(0); i++)
            {
                for (int j = 0; j < board.GetLength(0); j++)
                {
                    if (board[i, j] == 1 && !visited[i, j])
                    {
                        dfs(board, i, j, visited);
                    }
                }
            }
            return list;
        }

        private void dfs(int[,] board, int i, int j, bool[,] visited)
        {
            visited[i, j] = true;
            list.Add(new Result(i, j));
            int[] dx = { -1, 1, 0, 0, -1, -1, 1, 1 };
            int[] dy = { 0, 0, -1, 1, -1, 1, -1, 1 };
            for (int k = 0; k < dx.Length; k++)
            {
                int x = i + dx[k];
                int y = j + dy[k];
                // error case
                if (x < 0 || x >= board.GetLength(0) || y < 0 || y >= board.GetLength(1) || 
                    // if board is zero and already visted
                    board[x, y] == 0 || visited[x, y])
                {
                    //do nothing
                    continue;
                }
                dfs(board, x, y, visited);
            }
        }
    }
}
