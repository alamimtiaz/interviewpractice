﻿using System.Collections.Generic;
using System.Linq;

namespace GraphInterviewPractice.InterviewProblems
{
    /* Given a directed and connected graph with n nodes. If there is an edge from u to v 
     * then u depends on v. Our task was to find out the sum of dependencies for every node.
     * 
     * Example:
         For the graph in diagram,
         A depends on C and D i.e. 2
         B depends on D i.e. 1
         C depends on D i.e. 1
         And D depends on none.
         Hence answer -> 0 + 1 + 1 + 2 = 4
    */
    public class SumOfDependencies
    {

        // find the sum of all dependencies
        public int Solve(List<int>[] adj)
        {
            int sum = 0;

            // just find the size at each vector's index
            for (int u = 0; u < adj.Length-1; u++)
                sum += adj[u].Count();

            return sum;
        }

    }
}
