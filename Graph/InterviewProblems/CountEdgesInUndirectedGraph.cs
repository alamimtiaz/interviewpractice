﻿using System.Collections.Generic;
using System.Linq;

namespace GraphInterviewPractice.InterviewProblems
{
    class CountEdgesInUndirectedGraph
    {
        public int Solve(List<int>[] adj)
        {
            int sum = 0;

            //traverse all vertex
            for (int i = 0; i < adj.Length; i++)

                // add all edge that are linked to the
                // current vertex
                sum += adj[i].Count();

            // The count of edge is always even because in
            // undirected graph every edge is connected
            // twice between two vertices
            return sum / 2;

        }
    }
}
