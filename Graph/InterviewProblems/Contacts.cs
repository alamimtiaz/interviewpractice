﻿namespace GraphInterviewPractice.InterviewProblems
{
    public class TriesNode
    {
        private static int NUMBER_OF_CHARACTERS = 26;
        private TriesNode[] children = new TriesNode[NUMBER_OF_CHARACTERS];
        private bool IsCompleteWord { get; set; }
        int size = 0;

        private int getCharIndex(char c)
        {
            return c - 'a';
        }

        private TriesNode GetNode(char c)
        {
            return children[getCharIndex(c)];
        }

        private void SetCode(char c, TriesNode node)
        {
            children[getCharIndex(c)] = node;
        }

        public void Add(string s)
        {
            Add(s, 0);
        }

        private void Add(string s, int index)
        {
            if (index == s.Length) return;
            char current = s[index];
            TriesNode child = GetNode(current);
            if (child == null)
            {
                child = new TriesNode();
                SetCode(current, child);
            }

            child.Add(s, index + 1);
        }

        public int findCount(string s, int index)
        {
            if (index == s.Length) return size;
            TriesNode child = GetNode(s[index]);
            if (child == null) return 0;
            return child.findCount(s, index + 1);
        }
    }
}
