﻿using System.Collections.Generic;
using GraphInterviewPractice.Model;

namespace GraphInterviewPractice.InterviewProblems
{
    public class FindRouteBetweenTwoNodes
    {
        // it has used the BFS algorithm to find the matching node
        public bool Solve(Graph g, Node start, Node end)
        {
            if (start == end) return true;

            foreach (Node node in g.Nodes)
                node.State = State.Unvisited;

            Queue<Node> q = new Queue<Node>();

            start.State = State.Visiting;
            q.Enqueue(start);

            while(q.Count != 0)
            {
                Node currentNode = q.Dequeue();
                if(currentNode != null)
                {
                    foreach(Node node in currentNode.AdjacentNodes)
                    {
                        if(node.State == State.Unvisited)
                        {
                            if (node == end)
                                return true;
                            else
                            {
                                node.State = State.Visiting;
                                q.Enqueue(node);
                            }
                        }
                    }
                    currentNode.State = State.Visited;
                }
            }

            return false;
        }
    }
}
