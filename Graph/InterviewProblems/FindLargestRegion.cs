﻿using System;

namespace GraphInterviewPractice.InterviewProblems
{
    /*
     * Consider a matrix with rows and columns, where each cell contains either a ‘0’ or a ‘1’ and any cell 
     * containing a 1 is called a filled cell. Two cells are said to be connected if they are adjacent to 
     * each other horizontally, vertically, or diagonally .If one or more filled cells are also connected, 
     * they form a region. find the length of the largest region.
     * 
     * Input : M[][5] = { 0 0 1 1 0
                   1 0 1 1 0
                   0 1 0 0 0
                   0 0 0 0 1 }
        Output : 6 
        Ex: in the following example, there are 2 regions one with length 1 and the other as 6.
        so largest region : 6
     */

    public class FindLargestRegion
    {
        public int Solve(int[,] board)
        {
            int result = Int32.MinValue;
            if (board == null || board.GetLength(0) < 2 || board.GetLength(1) < 2)
                return result;
            
            bool[,] visited = new bool[board.Length, board.GetLength(0)];
            for (int i = 0; i < board.GetLength(0); i++)
            {
                for (int j = 0; j < board.GetLength(0); j++)
                {
                    if (board[i, j] == 1 && !visited[i, j])
                    {
                        int count = 1;
                        dfs(board, i, j, visited, ref count);
                        result = Math.Max(result, count);
                    }
                }
            }
            return result;
        }


        private void dfs(int[,] board, int i, int j, bool[,] visited, ref int count)
        {
            visited[i, j] = true;

            int[] dx = { -1, 1, 0, 0, -1, -1, 1, 1 };
            int[] dy = { 0, 0, -1, 1, -1, 1, -1, 1 };
            for (int k = 0; k < dx.Length; k++)
            {
                int x = i + dx[k];
                int y = j + dy[k];
                if (x < 0 || x >= board.GetLength(0) || y < 0 || y >= board.GetLength(0) || board[x, y] == 0 || visited[x, y])
                {
                    continue;
                }
                count++;
                dfs(board, x, y, visited, ref count);
            }
        }
    }
}
