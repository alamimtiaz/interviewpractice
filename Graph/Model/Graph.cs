﻿using System.Collections.Generic;

namespace GraphInterviewPractice.Model
{
    public class Graph
    {
        public List<Node> Nodes { get; set; }

        public int Size
        {
            get
            {
                return this.Nodes.Count;
            }
        }
        
        public List<Node> GetAllNodes()
        {
            // TODO: Do the traverse of whole graph
            return new List<Node>();
        }
    }
}
