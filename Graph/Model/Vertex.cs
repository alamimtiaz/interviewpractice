﻿using System;

namespace GraphInterviewPractice.Model
{
    public class Vertex
    {
        public String Id { get; set; }
        public String Name { get; set; }
        public VistedState State { get; set; }

        public enum VistedState
        {
            Unvisited,
            Visiting,
            Visited
        }
    }
}
