﻿using System;

namespace GraphInterviewPractice.Model
{
    public class Edge
    {
        public  String Id { get; set; }
        public  Vertex Source { get; set; }
        public  Vertex Destination { get; set; }
        public  int Weight { get; set; }
    }
}
