﻿using System.Collections.Generic;

namespace GraphInterviewPractice.Model
{
    public class Node
    {
        public int Data { get; set; }
        public State State { get; set; }

        public List<Node> AdjacentNodes { get; set; }
    }

    public enum State
    {
        Unvisited,
        Visiting,
        Visited
    }
}
