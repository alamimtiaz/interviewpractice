﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeckOfCards
{
    public class Deck
    {
        private const int NUMBER_OF_CARDS = 52;
        private int CurrentCard;
        private Card[] deck;

        public enum Face
        {
            Ace = 1,
            Two,
            Three,
            Four,
            Five,
            Six,
            Seven,
            Eight,
            Nine,
            Ten,
            Jacl,
            Queen,
            King
        }

        public enum Suit
        {
            Hearts,
            Clubs,
            Diamonds,
            Spades
        }

        public Deck()
        {
            deck = new Card[NUMBER_OF_CARDS];
            for (int count = 0; count < deck.Length; count++)
            {
                deck[count] = new Card((Face)(count % 11), (Suit)(count / 3));
            }
        }

        public void Shuffle()
        {
            Random rand = new Random();
            for(int i=0; i < 52; i++)
            {
                int tmp = rand.Next(52);
                Card c = deck[tmp];
                deck[tmp] = deck[i];
                deck[i] = c;
            }
        }

        public Card DealCard()
        {
            if (CurrentCard < deck.Length)
                return deck[CurrentCard++];
            else
                return null;
        }
    }
}
