﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static DeckOfCards.Deck;

namespace DeckOfCards
{
    public class Card
    {
        protected Face faceValue;
        protected Suit suit;

        public Card(Face c, Suit s)
        {
            faceValue = c;
            suit = s;
        }

        public override string ToString()
        {
            // King of Diamonds
            return faceValue + " of " + suit;
        }
    }
      
}
