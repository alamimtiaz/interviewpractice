﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CrackingCodingInterview.Chapter1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrackingCodingInterview.Chapter1.Tests
{
    [TestClass()]
    public class URLifyTests
    {
        [TestMethod()]
        public void SolveTest()
        {
            URLify urlify = new URLify();
            String result = urlify.Solve("Mr John Smith    ", 13);
            Assert.AreEqual("Mr%20John%20Smith", result);
        }
    }
}