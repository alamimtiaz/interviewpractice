﻿namespace TreeInterviewPractice.DataStructures
{
    public class PriorityQueue<T>
    {
        private MinIntHeap _heap;

        public PriorityQueue()
        {
            _heap = new MinIntHeap();
        }

        public void Push(int data)
        {
            _heap.Poll(data);
        }

        public int Peek()
        {
            return _heap.Peek();
        }

        public int Pop()
        {
            return _heap.Pop();
        }
    }
}
