﻿using System;

namespace TreeInterviewPractice.DataStructures
{
    public class MaxIntHeap
    {
        private int capacity = 100;
        private int size;

        int[] items = new int[100];

        
        public void Poll(int item)
        {
            EnsureExtraCapacity();
            items[size] = item;
            size++;
            HeapifyUp();
        }

        public int Peek()
        {
            if (size == 0) throw new ArgumentException();
            return items[0];
        }

        public int Pop()
        {
            if (size == 0) throw new ArgumentException();
            int item = items[0];
            items[0] = items[size - 1];
            HeapifyDown();
            size--;
            return item;
        }

        public void Print()
        {
            for (int i = 0; i < size; i++)
            {
                Console.Write(items[i]);
                Console.Write(" ");
            }
            Console.WriteLine("");
        }

        public int Size
        {
            get { return size; }
        }

        #region Private methods
        private int GetLeftChildIndex(int parentIndex) { return 2 * parentIndex + 1; }
        private int GetRightChildIndex(int parentIndex) { return 2 * parentIndex + 2; }
        private int GetParentIndex(int childIndex) { return (childIndex - 1) / 2; }

        private bool HasLeftChild(int index) { return GetLeftChildIndex(index) < size; }
        private bool HasRightChild(int index) { return GetRightChildIndex(index) < size; }
        private bool HasParent(int index) { return GetParentIndex(index) >= 0; }

        private int LeftChild(int index) { return items[GetLeftChildIndex(index)]; }
        private int RightChild(int index) { return items[GetRightChildIndex(index)]; }
        private int Parent(int index) { return items[GetParentIndex(index)]; }

        private void Swap(int indexOne, int indexTwo)
        {
            int temp = items[indexOne];
            items[indexOne] = items[indexTwo];
            items[indexTwo] = temp;
        }

        private void EnsureExtraCapacity()
        {
            if (size == capacity)
            {
                capacity = capacity * 2;
                Array.Resize<int>(ref items, capacity);
            }
        }

        private void HeapifyUp()
        {
            int index = size - 1;
            while (HasParent(index) && Parent(index) < items[index])
            {
                Swap(GetParentIndex(index), index);
                index = GetParentIndex(index);
            }
        }

        private void HeapifyDown()
        {
            int index = 0;
            while (HasLeftChild(index))
            {
                int smallerChildIndex = GetLeftChildIndex(index);
                if (HasLeftChild(index) && RightChild(index) < LeftChild(index))
                {
                    smallerChildIndex = GetRightChildIndex(index);
                }

                if (items[index] > items[smallerChildIndex])
                {
                    break;
                }
                else
                {
                    Swap(index, smallerChildIndex);
                }
                index = smallerChildIndex;
            }
        }
        #endregion
    }
}
