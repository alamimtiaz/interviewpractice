﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TreeApplication.DataStructures;

namespace TreeInterviewPractice.DataStructures
{
    public class Tries
    {
        readonly TrieNode root;

        public Tries()
        {
            root = new TrieNode();
        }

        public void Insert(String word)
        {
            TrieNode Current = root;
            for (int i = 0; i < word.Length; i++)
            {
                int index = word[i] - 'a';

                if (Current.Nodes[index] == null)
                {
                    Current.Nodes[index] = new TrieNode();
                }

                Current = Current.Nodes[index];
            }
            Current.EOW = true;
        }

        /** Returns if there is any word in the trie that starts with the given prefix. */
        public bool StartsWith(string prefix)
        {
            TrieNode Current = root;
            TrieNode Node = null;
            for (int i = 0; i < prefix.Length; i++)
            {
                int index = prefix[i] - 'a';
                if (Current.Nodes[index] == null)
                {
                    break;
                }
                Node = Current.Nodes[index];
            }
            return Node != null;
        }
    }
}

