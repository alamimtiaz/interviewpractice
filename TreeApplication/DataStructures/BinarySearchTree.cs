﻿using System;
using System.Collections.Generic;
using TreeInterviewPractice.InterviewProblems;
using TreeInterviewPractice.Model;

namespace TreeInterviewPractice.DataStructures
{
    public class BinarySearchTree
    {
        public Node Root { get; set; }

        public void Insert(int data)
        {
            Node newNode = new Node(data);
            Root = Insert(Root, newNode);
        }

        private Node Insert(Node currentParent, Node newNode)
        {
            // when null, it is the place to add the node
            if (currentParent == null) 
            {
                return newNode;
            }
            // when data to be inserted is larger than current node, the node to be inserted in right branch
            else if (newNode.Data > currentParent.Data) 
            {
                currentParent.Right = Insert(currentParent.Right, newNode);
            }
            // when data to be inserted is smaller than current node, the node to be inserted in left branch
            else if (newNode.Data < currentParent.Data)
            {
                currentParent.Left = Insert(currentParent.Left, newNode);
            }

            return currentParent;
        }

        public void Remove(int data)
        {
            RemovePrivate(Root, data);
        }
        private void RemovePrivate(Node root, int data)
        {
            if (root == null) return;
            else if (data < root.Data) RemovePrivate(root.Left, data);
            else if (data > root.Data) RemovePrivate(root.Right, data);
            else
            {
                // Case 1: Has two Children
                if(root.Left == null && root.Right == null)
                {
                    Node result = FindMin(root.Right);
                    //BSTNode result = FindMax(root.Left); // It should work same as above
                    root.Data = result.Data;
                    RemovePrivate(root.Right, result.Data);
                }
                // Case 2: Has only left child
                else if (root.Left != null)
                {
                    Node trash = root;
                    root = root.Left;
                    trash = null;
                }
                // Case 3: Has only right child
                else if (root.Right != null)
                {
                    Node trash = root;
                    root = root.Right;
                    trash = null;
                }
                // Case 4: Has no child
                else
                {
                    root = null;
                }
            }
        }

        public Node Search(Node current, int key)
        {
            // when not found ...
            if (current == null)
                return null;
            // found, Yahoo ......
            else if (current.Data == key)
                return current;
            // key is larger than current node value, so the desired node must be on right branch
            else if (key > current.Data)
                return Search(current.Right, key);
            // otherwise, key is smaller or equal than current node value, so the desired node must be on left branch
            else
                return Search(current.Left, key);
        }

        public int Size(Node current)
        {
            if (current == null) return 0;

            int leftSize = Size(current.Left);
            int rightSize = Size(current.Right);
            return leftSize + rightSize + 1;
        }

        public int Height(Node current)
        {
            if (current == null) return 0;

            int leftHeight = Height(current.Left);
            int rightHeight = Height(current.Right);
            return 1 + Math.Max(leftHeight, rightHeight);
        }

        public Node FindMin(Node current)
        {
            if (current == null) return null;

            while(current.Left != null)
            {
                current = current.Left;
            }

            return current;
        }

        public Node FindMinRecurse(Node current)
        {
            if (current == null)
                return null;
            else if (current.Left == null)
                return current;

            return FindMinRecurse(current.Left);
        }

        public Node FindMax(Node current)
        {
            if (current == null) return null;

            while(current.Right != null)
            {
                current = current.Right;
            }

            return current;
        }

        public Node FindMaxRecurse(Node current)
        {
            if (current == null)
                return null;
            else if (current.Right == null)
                return current;

            return FindMaxRecurse(current.Right);
        }

        public void PreOrderTraverse()
        {
            PreOrderTraversePrivate(Root);
        }

        private void PreOrderTraversePrivate(Node current)
        {
            if(current != null)
            {
                Console.WriteLine(current.Data);
                PreOrderTraversePrivate(current.Left);
                PreOrderTraversePrivate(current.Right);
            }
        }

        public void InOrderTraverse()
        {
            InOrderTraversePrivate(Root);
        }

        private void InOrderTraversePrivate(Node current)
        {
            if(current != null)
            {
                InOrderTraversePrivate(current.Left);
                Console.WriteLine(current.Data);
                InOrderTraversePrivate(current.Right);
            }
        }

        public void PostOrderTraverse()
        {
            PostOrderTraversePrivate(Root);
        }

        private void PostOrderTraversePrivate(Node current)
        {
            if(current != null)
            {
                PostOrderTraversePrivate(current.Left);
                PostOrderTraversePrivate(current.Right);
                Console.WriteLine(current.Data);
            }
        }

        public bool IsBST(Node current, int min, int max)
        {
            if (current == null) return true;
            if (current.Data <= min || current.Data > max)
            {
                return false;
            }

            return IsBST(current.Left, min, current.Data) && IsBST(current.Right, current.Data, max);
        }

        public Node FindLCA(Node current, Node n1, Node n2)
        {
            if(current.Data > Math.Max(n1.Data, n2.Data))
            {
                return FindLCA(current.Right, n1, n2);
            }
            else if(current.Data < Math.Min(n1.Data, n2.Data))
            {
                return FindLCA(current.Left, n1, n2);
            }
            else
            {
                return current;
            }
        }

        public bool IsBalanced(Node current)
        {
            if (current == null) return true;
            int leftHeight = Height(current.Left);
            int rightHeight = Height(current.Right);

            return (Math.Abs(leftHeight-rightHeight) <= 1) && IsBalanced(current.Left) && IsBalanced(current.Right);
        }

        public bool IsIdentical(Node a, Node b)
        {
            if (a == null && b == null)
            {
                return true;
            }
            else if (a != null && b != null)
            {
                return a.Data == b.Data && 
                    IsIdentical(a.Left, b.Left) && 
                    IsIdentical(a.Right, b.Right);
            }
            else
            {
                return false;
            }
        }

        public bool HasPathSum(Node node, int sum)
        {
            // return true if we run out of tree and sum==0
            if (node == null)
            {
                return sum == 0;
            }
            else
            {
                // otherwise check both subtrees
                return (HasPathSum(node.Left, sum - node.Data) || HasPathSum(node.Right, sum - node.Data));
            }
        }

        public bool IsSubtree(Node t1, Node t2)
        {
            if (t2 == null) return true;
            if (t1 == null) return false;
            if (IsIdentical(t1, t2)) return true;

            return IsSubtree(t1.Left, t2) && IsSubtree(t1.Right, t2);
        }

        // helper method to get the count of the leaves
        public int GetLeafCounts()
        {
            return GetLeafCounts(Root);
        }

        // get the number of leaf counts
        private int GetLeafCounts(Node current)
        {
            if (current == null) return 0;
            if (current.Left == null && current.Right == null) return 1;

            return GetLeafCounts(current.Left) + GetLeafCounts(current.Right);
        }

        public void PrintNodeWithinRange(int start, int end)
        {
            PrintNodeWithinRange(Root, start, end);
        }

        // It prints all the nodes within the range of two numbers
        private void PrintNodeWithinRange(Node current, int start, int end)
        {
            if (current == null) return;
            if (current.Data >= start && current.Data <= end)
                Console.WriteLine(current.Data);
            else if (current.Data < end)
                PrintNodeWithinRange(current.Left, start, end);
            else if(current.Data > start)
                PrintNodeWithinRange(current.Right, start, end);
        }

        public void MakeFlatten()
        {
            MakeTreeFlat makeFlat = new MakeTreeFlat();
            makeFlat.MakeFlatten(Root);
        }

        public void bfs()
        {
            bfs(Root);
        }
        private void bfs(Node root)
        {
            Queue<Node> queue = new Queue<Node>();

            queue.Enqueue(root);

            while(queue.Count != 0)
            {
                Node current = queue.Dequeue();
                Console.WriteLine(current.Data);

                if (current.Left != null)
                    queue.Enqueue(current.Left);
                if (current.Right != null)
                    queue.Enqueue(current.Right);
            }
        }

        public void dfs()
        {
            dfs(Root);
        }

        private void dfs(Node root)
        {
            if (root == null) return;

            Console.WriteLine(root.Data);
            dfs(root.Left);
            dfs(root.Right);
        }
    }
}
