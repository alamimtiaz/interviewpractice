﻿namespace TreeApplication.DataStructures
{
    public class TrieNode
    {
        public TrieNode[] Nodes { get; set; }
        public bool EOW { get; set; }
        public TrieNode()
        {
            Nodes = new TrieNode[26];
            EOW = false;
        }
    }
}