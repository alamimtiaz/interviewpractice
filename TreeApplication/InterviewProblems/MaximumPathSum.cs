﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TreeInterviewPractice.Model;

namespace TreeInterviewPractice.InterviewProblems
{
    public class MaximumPathSum
    {
        int max = int.MinValue;

        public int MaxPathSum(Node root)
        {
            dfsPathSum(root);
            return max;
        }

        private int dfsPathSum(Node root)
        {
            if (root == null) return 0;
            int left = Math.Max(dfsPathSum(root.Left), 0);
            int right = Math.Max(dfsPathSum(root.Right), 0);
            max = Math.Max(max, root.Data + left + right);
            return Math.Max(left, right) + root.Data;
        }
    }
}
