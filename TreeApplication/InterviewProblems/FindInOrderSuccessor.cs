﻿using TreeInterviewPractice.Model;

namespace TreeInterviewPractice.InterviewProblems
{
    public class FindInOrderSuccessor
    {
        public Node InOrderSuccessor(Node node)
        {
            if(node.Right != null) // return leftmost node of right tree
            {
                return LeftMostChild(node.Right);
            }
            else // return top-most parent
            {
                Node parent = node.Parent;

                while (parent != null && parent.Left != node)
                {
                    parent = parent.Parent;
                }

                return parent;
            }
        }

        private Node LeftMostChild(Node node)
        {
            if (node == null) return null;
            while(node.Left != null)
            {
                node = node.Left;
            }

            return node;
        }
    }
}
