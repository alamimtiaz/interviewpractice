﻿using System.Collections.Generic;

using TreeInterviewPractice.Model;

namespace TreeInterviewPractice.InterviewProblems
{
    public class FindParent
    {
        public Node Solve(Node root, int target)
        {
            if (root == null || root.Data == target) return null;

            Queue<Node> queue = new Queue<Node>();
            queue.Enqueue(root);

            while(queue.Count != 0)
            {
                Node parent = queue.Dequeue();

                if(parent.Left != null)
                {
                    if (parent.Left.Data == target)
                        return parent;
                    queue.Enqueue(parent.Left);
                }

                if (parent.Right != null)
                {
                    if (parent.Right.Data == target)
                        return parent;
                    queue.Enqueue(parent.Right);
                }
            }

            return null;
        }
    }
}
