﻿using TreeInterviewPractice.Model;

namespace TreeInterviewPractice.InterviewProblems
{
    public class MakeTreeFlat
    {
        public void MakeFlatten(Node root)
        {
            if (root == null) return;

            Node left = root.Left;
            Node right = root.Right;

            root.Left = null;
            MakeFlatten(left);
            MakeFlatten(right);

            root.Right = left;
            Node current = root;
            while (current.Right != null)
                current = current.Right;
            current.Right = right;
        }
    }
}
