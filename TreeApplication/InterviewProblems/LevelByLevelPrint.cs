﻿using System;
using System.Collections.Generic;

using TreeInterviewPractice.Model;

namespace TreeInterviewPractice.InterviewProblems
{
    // Source: https://www.youtube.com/watch?v=7uG0gLDbhsI
    public class LevelByLevelPrint
    {
        public void Solve(Node root)
        {
            Queue<Node> queue = new Queue<Node>();
            root.Level = 0;
            queue.Enqueue(root);

            while(queue.Count != 0)
            {
                Node currentNode = queue.Dequeue();
                Console.Write(currentNode.Data + " ");

                Node nextNode = queue.Peek();
                if(currentNode.Level != nextNode.Level)
                {
                    Console.WriteLine("");
                }
                
                if(currentNode.Left != null)
                {
                    Node left = currentNode.Left;
                    left.Level = currentNode.Level + 1;
                    queue.Enqueue(left);
                }

                if(currentNode.Right != null)
                {
                    Node right = currentNode.Right;
                    right.Level = currentNode.Level + 1;
                    queue.Enqueue(right);
                }
            }
        }
    }
}
