﻿using System.Collections.Generic;

using TreeInterviewPractice.Model;

namespace TreeInterviewPractice.InterviewProblems
{
    // Source: https://www.youtube.com/watch?v=wrgMF-CuA8w
    // Question: Can we implement without level 
    public class ConnectNodesAtSameLevel
    {
        public void Solve(ConnectingNode root)
        {
            Queue<ConnectingNode> queue = new Queue<ConnectingNode>();
            root.Level = 0;

            queue.Enqueue(root);

            while(queue.Count != 0)
            {
                ConnectingNode currentNode = queue.Dequeue();

                // pointing to next right node
                ConnectingNode rightNode = queue.Peek();
                if (currentNode.Level == rightNode.Level)
                    currentNode.NextRight = rightNode;

                // add left node
                if (currentNode.Left != null)
                {
                    ConnectingNode left = currentNode.Left;
                    left.Level = currentNode.Level + 1;
                    queue.Enqueue(left);
                }

                // add right node
                if (currentNode.Right != null)
                {
                    ConnectingNode right = currentNode.Right;
                    right.Level = currentNode.Level + 1;
                    queue.Enqueue(right);
                }
            }
        }
    }
}
