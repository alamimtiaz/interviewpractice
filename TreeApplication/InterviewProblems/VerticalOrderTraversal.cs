﻿using System.Collections.Generic;

using TreeInterviewPractice.Model;

namespace TreeInterviewPractice.InterviewProblems
{
    // Source: https://www.youtube.com/watch?v=PQKkr036wRc
    public class VerticalOrderTraversal
    {
        private Dictionary<int, List<Node>> hash = new Dictionary<int, List<Node>>();

        public void Solve(Node root)
        {
            Queue<Node> queue = new Queue<Node>();
           

            root.HorizontalDistance = 0;
            // adding to hash
            AddToHashTable(root);
            queue.Enqueue(root);

            while(queue.Count != 0)
            {
                Node current = queue.Dequeue();

                if(current.Left != null)
                {
                    Node left = current.Left;
                    left.HorizontalDistance = current.HorizontalDistance - 1;
                    AddToHashTable(left);
                    queue.Enqueue(left);
                }

                if(current.Right != null)
                {
                    Node right = current.Right;
                    right.HorizontalDistance = current.HorizontalDistance + 1;
                    AddToHashTable(right);
                    queue.Enqueue(right);
                }
            }

            // Print all hash table now ....
        }

        private void AddToHashTable(Node node)
        {
            int key = node.HorizontalDistance;
            if (hash.ContainsKey(key))
            {
                List<Node> value = new List<Node>();
                if (hash.TryGetValue(key, out value))
                {
                    value.Add(node);
                }
            }
            else
            {
                var value = new List<Node>();
                value.Add(node);
                hash.Add(key, value);
            }
        }
    }
}
