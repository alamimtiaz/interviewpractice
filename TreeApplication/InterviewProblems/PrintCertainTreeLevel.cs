﻿using System;
using System.Collections.Generic;

using TreeInterviewPractice.Model;

namespace TreeInterviewPractice.InterviewProblems
{
    public class PrintCertainTreeLevel
    {
        // Source: https://www.youtube.com/watch?v=79fPL0F_1XA&t=331s
        public void PrintTreeLevel(Node node, int level)
        {
            if (level < 0) return;

            // initialize queues
            Queue<Node> trees = new Queue<Node>();
            Queue<int> levels = new Queue<int>();

            trees.Enqueue(node);
            levels.Enqueue(0);

            while (trees.Count != 0)
            {
                Node tempNode = trees.Dequeue();
                int currentLevel = levels.Dequeue();

                if (tempNode == null) return;
                else if (currentLevel == level) Console.Write(tempNode.Data);
                else
                {
                    trees.Enqueue(tempNode.Left); levels.Enqueue(currentLevel + 1);
                    trees.Enqueue(tempNode.Right); levels.Enqueue(currentLevel + 1);
                }
            }
        }
    }
}
