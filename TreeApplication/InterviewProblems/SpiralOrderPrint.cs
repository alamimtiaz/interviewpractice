﻿using System;
using System.Collections.Generic;

using TreeInterviewPractice.Model;

namespace TreeInterviewPractice.InterviewProblems
{
    // Source: https://www.youtube.com/watch?v=YsLko6sSKh8
    // Will be used to two stacks to implement
    public class SpiralOrderPrint
    {
        public void Solve(Node root)
        {
            Stack<Node> stack1 = new Stack<Node>();
            Stack<Node> stack2 = new Stack<Node>();

            stack1.Push(root);

            while(stack1.Count != 0 || stack2.Count != 0)
            {
                while(stack1.Count != 0)
                {
                    Node node = stack1.Pop();
                    Console.Write(node.Data + " ");

                    if (node.Left != null) stack2.Push(node.Left);
                    if (node.Right != null) stack2.Push(node.Right);
                }

                while(stack2.Count != 0)
                {
                    Node node = stack2.Pop();
                    Console.Write(node.Data + " ");

                    if (node.Right != null) stack1.Push(node.Right);
                    if (node.Left != null) stack1.Push(node.Left);
                }
            }
        }
    }
}
