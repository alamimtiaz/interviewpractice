﻿using TreeInterviewPractice.Model;

namespace TreeInterviewPractice.InterviewProblems
{
    public class CreateMinimalBST
    {
        public Node CreateMinimalTree(int[] array, int start, int end)
        {
            if (end < start) return null;

            int mid = (start + end) / 2;

            Node node = new Node(array[mid]);
            node.Left = CreateMinimalTree(array, start, mid - 1);
            node.Right = CreateMinimalTree(array, mid + 1, end);
            return node;
        }
    }
}
