﻿using TreeInterviewPractice.Model;

namespace TreeInterviewPractice.InterviewProblems
{
    public class IsTreeMirror
    {
        public bool IsMirror(Node node)
        {
            if (node == null) return true;
            return IsMirror(node.Left, node.Right);
        }

        private bool IsMirror(Node left, Node right)
        {
            if (left == null && right == null) return true;
            if (left == null || right == null) return false;
            if (left.Data != right.Data) return false;
            return IsMirror(left.Left, right.Right) && IsMirror(left.Right, right.Left);
        }
    }
}
