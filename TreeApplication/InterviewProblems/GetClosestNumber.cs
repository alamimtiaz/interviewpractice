﻿using System;

using TreeInterviewPractice.Model;

namespace TreeInterviewPractice.InterviewProblems
{
    public class GetClosestNumberImpl
    {
        // Source: https://www.youtube.com/watch?v=NMdMIrHEA1E
        public int GetClosestNumber(Node node, int target)
        {
            int minimumDifference = GetMinimumDifference(node, target);
            return target + minimumDifference;
        }

        public int GetMinimumDifference(Node node, int target)
        {
            if (node == null) return int.MaxValue;

            if (node.Data < target)
                return GetSmallNumber(node.Data - target, GetMinimumDifference(node.Right, target));
            else
                return GetSmallNumber(node.Data - target, GetMinimumDifference(node.Left, target));
        }

        private int GetSmallNumber(int a, int b)
        {
            if (Math.Abs(a) > Math.Abs(b))
                return b;
            return a;
        }
    }
}
