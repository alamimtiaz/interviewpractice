﻿using System;
using System.Collections.Generic;

using TreeInterviewPractice.Model;
using TreeInterviewPractice.DataStructures;

namespace TreeInterviewPractice.InterviewProblems
{
    // Source: https://www.youtube.com/watch?v=VmogG01IjYc
    public class FindRunningMedianUsingHeaps
    {
        public double[] getMedians(int[] array)
        {
            MaxIntHeap lowers = new MaxIntHeap();
            MinIntHeap highers = new MinIntHeap();
            double[] medians = new double[array.Length];
            
            for(int i = 0; i < array.Length; i++)
            {
                int number = array[i];
                AddNumber(number, lowers, highers);
                Rebalance(lowers, highers);
                medians[i] = GetMedian(lowers, highers);
            }

            return medians;
        }

        private void AddNumber(int number, MaxIntHeap lowers, MinIntHeap highers)
        {
            if (lowers.Size == 0 || number < lowers.Peek())
                lowers.Poll(number);
            else
                highers.Poll(number);
        }

        private void Rebalance(MaxIntHeap lowers, MinIntHeap highers)
        {
            if (lowers.Size > highers.Size && (lowers.Size - highers.Size) >= 2)
                highers.Poll(lowers.Pop());
            else if (highers.Size > lowers.Size && (highers.Size - lowers.Size) >= 2)
                lowers.Poll(highers.Pop());
        }

        private double GetMedian(MaxIntHeap lowers, MinIntHeap highers)
        {
            if (highers.Size == lowers.Size)
                return (double)(highers.Peek() + lowers.Peek()) / 2;
            else if (highers.Size > lowers.Size)
                return highers.Peek();
            else
                return lowers.Peek();
        }
    }
}
