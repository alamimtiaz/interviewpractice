﻿namespace TreeInterviewPractice.Model
{
    public class Node
    {
        private int data;
        public Node Left { get; set; }
        public Node Right { get; set; }
        public Node Parent { get; set; }
        public int Level { get; set; }
        public int HorizontalDistance { get; set; }

        public Node(int data)
        {
            this.data = data;
        }

        public override string ToString()
        {
            return data.ToString();
        }

        public int Data
        {
            get
            {
                return data;
            }
            set
            {
                data = value;
            }
        }

        
    }
}
