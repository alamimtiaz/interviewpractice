﻿namespace TreeInterviewPractice.Model
{
    public class ConnectingNode
    {
        private int data;
        public ConnectingNode Left { get; set; }
        public ConnectingNode Right { get; set; }
        public ConnectingNode NextRight { get; set; }
        public int Level { get; set; }

        public ConnectingNode(int data)
        {
            this.data = data;
        }

        public override string ToString()
        {
            return data.ToString();
        }

        public int Data
        {
            get
            {
                return data;
            }
            set
            {
                data = value;
            }
        }
    }
}
