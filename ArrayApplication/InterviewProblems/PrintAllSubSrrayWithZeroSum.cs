﻿using System;
using System.Collections.Generic;


namespace ArrayApplication.InterviewProblems
{
    /*
     * Given an array of integers, print all subarrays having 0 sum.
 

        For example, 

        Input:  { 4, 2, -3, -1, 0, 4 }
        Sub-arrays with 0 sum are
        { -3, -1, 0, 4 }
        { 0 }
 
        Input:  { 3, 4, -7, 3, 1, 3, 1, -4, -2, -2 }
        Sub-arrays with 0 sum are
        { 3, 4, -7 }
        { 4, -7, 3 }
        { -7, 3, 1, 3 }
        { 3, 1, -4 }
        { 3, 1, 3, 1, -4, -2, -2 }
        { 3, 4, -7, 3, 1, 3, 1, -4, -2, -2 }
    */
    public class PrintAllSubSrrayWithZeroSum
    {
        // Function to print all sub-arrays with 0 sum present
        // in the given array
        public List<List<int>> PrintAllSubSrrayWithZeroSum_Naive(int[] arr)
        {
            if (arr == null) return null;

            List<List<int>> result = new List<List<int>>();

            // n is length of the array
            int n = arr.Length;

            // consider all sub-arrays starting from i
            for (int i = 0; i < n; i++)
            {
                int sum = 0;

                // consider all sub-arrays ending at j
                for (int j = i; j < n; j++)
                {
                    // sum of elements so far
                    sum += arr[j];

                    // if sum is seen before, we have found a sub-array with 0 sum
                    if (sum == 0)
                        result.Add(GetList(arr, i, j));
                }
            }
            return result;
        }

        private List<int> GetList(int[] arr, int i, int j)
        {
            List<int> result = new List<int>();
            for (int x = i; x <= j; x++)
            {
                result.Add(arr[x]);
            }
            return result;
        }

        public int[] FindZeroSum(int[] array)
        {
            Dictionary<int, int> sums = new Dictionary<int, int>();
            int sum = 0;

            for (int i = 0; i <= array.Length; i++)
            {
                bool oldIndexExists = sums.ContainsKey(sum);
                if (!oldIndexExists && i == array.Length)
                    return null;
                else if (!oldIndexExists)
                {
                    sums.Add(sum, i);
                    sum += array[i];
                }
                else
                {
                    int oldIndex = sums[sum];
                    int[] result = null;
                    Array.Copy(array, oldIndex, result, 0, i - oldIndex);
                    return result;
                }
            }
            return null;
        }
    }
}
