﻿using System;

namespace ArrayInterviewPractice.InterviewProblems
{
    public class CalculateMaximumProfit
    {
        public int Solve(int[] prices)
        {
            int maxPrice = 0;
            int maxProfit = 0;

            for(int i = prices.Length - 1; i >= 0 ; i--)
            {
                maxPrice = Math.Max(maxPrice, prices[i]);
                maxProfit = Math.Max(maxProfit, maxPrice - prices[i]);
            }

            return maxProfit;
        }
    }
}
