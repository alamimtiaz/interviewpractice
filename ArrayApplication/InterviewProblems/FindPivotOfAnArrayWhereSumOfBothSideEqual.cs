﻿namespace ArrayInterviewPractice.InterviewProblems
{
    public class FindPivotOfAnArrayWhereSumOfBothSideEqual
    {
        public int FindPivot(int[] array)
        {
            if (array == null || array.Length == 0)
                return -1;

            int leftSum = 0;
            int rightSum = 0;

            for (int i = 0; i < array.Length; i++)
                rightSum += array[i];

            for(int j = 0; j < array.Length; j++)
            {
                rightSum -= array[j];
                if (leftSum == rightSum)
                    return j;
                leftSum += array[j];
            }

            // not found ...
            return -1;
        }
    }
}
