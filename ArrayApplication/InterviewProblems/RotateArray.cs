﻿
namespace ArrayInterviewPractice.InterviewProblems
{
    public class RotateArray
    {
        public int[] Solve(int[] array, int num)
        {
            for(int i = 0; i < num; i++)
            {
                int destinationIndex = GetDestinationLocation(array.Length, num, i);
                int saved = array[destinationIndex];
                array[destinationIndex] = array[i];
                array[i] = saved;
            }

            return array;
        }

        private int GetDestinationLocation(int size, int rotationNumber, int index)
        {
            return (size - rotationNumber + index) % size;
        }
    }
}
