﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayApplication
{
    /*
     * Given an unsorted array of integers, find a pair with given sum in it.
 
        For example, 

        Input:
        arr = [8, 7, 2, 5, 3, 1]
        sum = 10
 
        Output: 
        Pair found at index 0 and 2 (8 + 2)
        OR
        Pair found at index 1 and 5 (7 + 3)
     * 
     */
    public class FindPairWithGivenSumInTheArray
    {
        // Naive solution would be to consider every pair in given array and return if desired sum is found.
        public int[] FindPair_Naive(int[] arr, int sum)
        {
            if (arr == null || arr.Length <2) return null;

            // n is length of the array
            int n = arr.Length;

            // consider each element except last element
            for (int i = 0; i < n - 1; i++)
            {
                // start from i'th element till last element
                for (int j = i + 1; j < n; j++)
                {
                    // if desired sum is found, print it and return
                    if (arr[i] + arr[j] == sum)
                    {

                        return new int[] { i, j };
                    }
                }
            }
            return null;
        }

        /*
        O(nlogn) solution using Sorting –
 
        The idea is to sort the given array in ascending order and maintain search space 
        by maintaining two indexes(low and high) that initially points to two end-points 
        of the array.Then we loop till low is less than high index and reduce search 
        space arr[low..high] at each iteration of the loop.We compare sum of elements 
        present at index low and high with desired sum and increment low if sum is less 
        than the desired sum else we decrement high is sum is more than the desired sum.
        Finally, we return if pair found in the array.
        */
        public int[] FindPair_NLogN(int[] arr, int sum)
        {
            if (arr == null || arr.Length < 2) return null;

            // sort the array in ascending order
            Array.Sort(arr);

            // maintain two indexes pointing to end-points of the array
            int low = 0;
            int high = arr.Length - 1;

            // reduce search space arr[low..high] at each iteration of the loop

            // loop till low is less than high
            while (low < high)
            {
                // sum found
                if (arr[low] + arr[high] == sum)
                {
                    return new int[] { low, high };
                }

                // increment low index if total is less than the desired sum
                // decrement high index is total is more than the sum
                if (arr[low] + arr[high] < sum)
                    low++;
                else
                    high--;
            }
            return null;
        }

        /*
        O(n) solution using Hashing –
        We can use map to easily solve this problem in linear time. The idea is to 
        insert each element of the array arr[i] in a map. We also checks if difference 
        (arr[i], sum-arr[i]) already exists in the map or not. If the difference is seen 
        before, we print the pair and return.
        */

        public int[] FindPair_Hashing(int[] arr, int sum)
        {
            if (arr == null || arr.Length < 2) return null;

            // create an empty Hash Map
            Dictionary<Int32, Int32> map = new Dictionary<Int32, Int32>();
            
            // do for each element
            for (int i = 0; i < arr.Length; i++)
            {
                // if difference is seen before, return the pair
                if (map.ContainsKey(sum - arr[i]))
                {
                    return new int[] { map[sum - arr[i]], i };
                }

                // Add to dictionary only when it doesn't exists
                // to prevent key already exists error
                if(!map.ContainsKey(arr[i]))
                    map.Add(arr[i], i);
            }
            return null;
        }
    }
}