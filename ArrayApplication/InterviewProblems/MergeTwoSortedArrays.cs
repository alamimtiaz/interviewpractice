﻿namespace ArrayInterviewPractice.InterviewProblems
{
    // Merge two sorted arrays without additional memory
    public class MergeTwoSortedArrays
    {
        public int Merge(int[] longArray, int[] shortArray, int longUsed)
        {
            if (longArray == null
                || shortArray == null
                || shortArray.Length + longUsed > longArray.Length
                || longUsed < 0
                || longUsed > longArray.Length)
            {
                return -1;
            }

            int longTail = longUsed - 1;
            int shortTail = shortArray.Length - 1;

            while(longTail >= 0 && shortTail >= 0)
            {
                if(longArray[longTail] > shortArray[shortTail])
                {
                    longArray[longTail + shortTail + 1] = longArray[longTail];
                    longTail--;
                }
                else
                {
                    longArray[longTail + shortTail + 1] = shortArray[shortTail];
                    shortTail--;
                }
            }

            while(shortTail >= 0)
            {
                longArray[shortTail] = shortArray[shortTail];
                shortTail--;
            }

            return shortArray.Length + longUsed;
        }
    }
}
