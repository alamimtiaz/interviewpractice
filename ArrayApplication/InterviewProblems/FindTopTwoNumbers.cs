﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayApplication.InterviewProblems
{
    public class FindTopTwoNumbers
    {
        public void PrintTopTwoNumbers(int[] numbers)
        {
            int max1 = int.MinValue;
            int max2 = int.MinValue;

            for(int i = 0; i < numbers.Length; i++)
            {
                if (numbers[i] > max1)
                {
                    max2 = max1;
                    max1 = numbers[i];
                }
                else if (numbers[i] > max2)
                {
                    max2 = numbers[i];
                }
            }
        }
    }
}
