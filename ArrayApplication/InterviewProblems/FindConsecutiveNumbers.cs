﻿using System;
using System.Collections.Generic;

namespace ArrayInterviewPractice.InterviewProblems
{
    public class FindConsecutiveNumbers
    {
        // Using Sort
        // O(NLogN)
        public int FinMaxConsecutiveNumbers(int[] array)
        {
            if (array == null || array.Length == 0)
                return -1;

            int maxLength = 1, curLength = 1;
            Array.Sort<int>(array);
            int prev = array[0];
            for (int i = 1; i < array.Length; i++)
            {
                if (prev == array[i] - 1)
                {
                    curLength++;
                }
                else
                {
                    maxLength = Math.Max(maxLength, curLength);
                    curLength = 1;
                }
                prev = array[i];
            }
            return maxLength;
        }

        public int FinMaxConsecutiveNumbers2(int[] array)
        {
            int maxLength = 0;
            HashSet<int> hash = new HashSet<int>();
            for (int i = 0; i < array.Length; i++)
                hash.Add(array[i]);

            foreach (int number in hash)
            {
                int currentNumber = number;
                int currentLength = 0;
                while (hash.Contains(currentNumber))
                {
                    currentNumber++;
                    currentLength++;
                }
                maxLength = Math.Max(currentLength, maxLength);
                currentLength = 0;
            }

            return maxLength;
        }
    }
}