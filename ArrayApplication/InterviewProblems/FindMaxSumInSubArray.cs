﻿using System;

namespace ArrayApplication.InterviewProblems
{
    public class FindMaxSumInSubArray
    {
        //returns max sum, also finds the index
        public int Solve(int[] a)
        {
            int maxSum = Int32.MinValue;
            int max = a[0];
            int tempStartIndex = 0;
            int startIndex = 0;
            int endIndex = 0;
            for (int i = 1; i < a.Length; i++)
            {
                if (a[i] > max)
                {
                    max = a[i];
                    tempStartIndex = i;
                }
                else
                {
                    max += a[i];
                }
                if (max > maxSum)
                {
                    startIndex = tempStartIndex;
                    endIndex = i;
                    maxSum = max;
                    
                }
            }

            return maxSum;
        }
    }
}
