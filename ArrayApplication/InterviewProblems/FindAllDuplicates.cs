﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ArrayApplication.InterviewProblems
{
    public class FindAllDuplicates
    {
        public List<int> GetAllDuplicates(int[] array)
        {
            HashSet<int> hash = new HashSet<int>();

            for(int i = 0; i < array.Length; i++)
            {
                int index = Math.Abs(array[i]) - 1;

                if(array[index] < 0)
                {
                    hash.Add(Math.Abs(array[i]));
                }
                else
                {
                    array[index] = -array[index];
                }
            }

            for (int i = 0; i < array.Length; i++)
            {
                array[i] = Math.Abs(array[i]);
            }

            return hash.ToList<int>();
        }
    }
}
