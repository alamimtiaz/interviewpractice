﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayApplication.InterviewProblems
{
    // The set [1,2,3,…,n] contains a total of n! unique permutations.
    // By listing and labeling all of the permutations in order
    public class KthPermutations
    {
        public String Solve(int n, int k)
        {
            StringBuilder sb = new StringBuilder();
            List<Int32> num = new List<Int32>();

            for (int i = 1; i <= n; i++)
                num.Add(i);

            int fact = Factorial(num.Count);

            int rank = k - 1;
            for (int i = 0; i < n; i++)
            {
                fact /= (n - i);
                int index = (rank / fact);
                sb.Append(num[index]);
                num.RemoveAt(index);
                rank -= index * fact;
            }
            return sb.ToString();

        }

        private int Factorial(int n)
        {
            return (n <= 1) ? 1 : n * Factorial(n - 1);
        }
    }
}
