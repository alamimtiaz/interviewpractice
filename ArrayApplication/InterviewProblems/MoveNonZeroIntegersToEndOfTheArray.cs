﻿namespace ArrayInterviewPractice.InterviewProblems
{
    // Given an array of integers, design an algorithm that moves 
    // all non-zero integers to the end of the array
    public class MoveNonZeroIntegersToEndOfTheArray
    {
        public int[] MoveNonZeroElementsToEnd(int[] array)
        {
            if (array == null || array.Length == 0) return array;

            int i = 0;
            int j = array.Length - 1;

            while(true)
            {
                while(i < array.Length && array[i] == 0)
                {
                    i++;
                }
                    

                while (j >= 0 && array[j] != 0)
                {
                    j--;
                }

                if (i < j)
                    Swap(array, i, j);
                else
                    break;
            }

            return array;
        }

        private void Swap(int[] array, int i, int j)
        {
            int tmp = array[i];
            array[i] = array[j];
            array[j] = tmp;
        }
    }
}
