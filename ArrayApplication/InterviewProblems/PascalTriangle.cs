﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//[
//     [1],
//    [1,1],
//   [1,2,1],
//  [1,3,3,1],
// [1,4,6,4,1]
//]

namespace ArrayApplication.InterviewProblems
{
    public class PascalTriangle
    {
        public List<List<int>> Solve(int numberOfRows)
        {
            List<List<int>> result = new List<List<int>>();
            List<int> row = null;
            List<int> prev = null;

            for(int i = 0; i < numberOfRows; i++)
            {
                row = new List<int>();
                for (int j = 0; j < i; j++)
                {
                    if (j == 0)
                        row.Add(1);
                    else
                        row.Add(prev.ToArray<int>()[j - 1] + prev.ToArray<int>()[j]);
                }
            }

            return result;
        }
    }
}
