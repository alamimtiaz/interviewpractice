﻿using System;
using System.Collections.Generic;

namespace ArrayApplication.InterviewProblems
{
    public class ThreeSum
    {
        public List<int[]> GetThreeSumList(int[] array)
        {
            List<int[]> result = new List<int[]>();
            Array.Sort(array);

            for(int i = 0; i < array.Length; i++)
            {
                if(i == 0 || array[i] > array[i-1])
                {
                    int start = i + 1;
                    int end = array.Length - 1;

                    while(start < end)
                    {
                        if (array[i] + array[start] + array[end] == 0)
                            result.Add(new int[] { array[i], array[start], array[end] });

                        if(array[i] + array[start] + array[end] < 0)
                        {
                            int currentStart = start;
                            while(array[currentStart] == array[start] && start < end)
                                start++;
                        }
                        else
                        {
                            int currentEnd = end;
                            while (array[currentEnd] == array[end] && start < end)
                                end--;
                        }
                    }
                }
            }

            return result;
        }
    }
}
