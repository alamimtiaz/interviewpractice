﻿using System;

namespace ArrayInterviewPractice.InterviewProblems
{
    // This method provides you the arithemetic addition
    // input: { 6, 3, 4 }, { 3, 8, 5 };
    // output: {1, 0, 1, 9}
    public class AddTwoNumbers
    {
        private const int TEN_BASE = 10;
        
        // Arithemetic addition
        public int[] Sum(int[] a, int[] b)
        {
            if (a == null || b == null)
                return new int[0];

            int maxLength = GetMaxLength(a, b);
            int[] result = new int[maxLength + 1];

            int carry = 0;
            for (int i = 0; i < maxLength; i++)
            {
                int firstNumber = GetNumber(a, i);
                int secondNumber = GetNumber(b, i);

                int sum = firstNumber + secondNumber + carry;
                carry = sum / TEN_BASE;
                int digitToPutInOutput = sum % TEN_BASE;

                // put the calculated result to output array
                result[result.Length - 1 - i] = digitToPutInOutput;
            }
            if(carry != 0)
                result[0] = carry;

            return result;
        }

        // this method returns the number from right
        private int GetNumber(int[] a, int index)
        {
            if (index >= a.Length) 
                return 0;
            else
                return a[a.Length - 1 - index];
        }

        private int GetMaxLength(int[] a, int[] b)
        {
            return Math.Max(a.Length, b.Length);
        }
    }
}
