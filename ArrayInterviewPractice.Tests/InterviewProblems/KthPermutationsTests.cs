﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ArrayApplication.InterviewProblems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayApplication.InterviewProblems.Tests
{
    [TestClass()]
    public class KthPermutationsTests
    {
        [TestMethod()]
        public void GetKthPermutationsTest()
        {
            KthPermutations kp = new KthPermutations();
            String s = kp.Solve(2, 1);
            Assert.AreEqual("12",s);
        }
    }
}