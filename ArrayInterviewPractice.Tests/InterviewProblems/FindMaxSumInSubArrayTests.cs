﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ArrayApplication.InterviewProblems.Tests
{
    [TestClass()]
    public class FindMaxSumInSubArrayTests
    {
        [TestMethod()]
        public void FindMaxSumInSubArrayTest()
        {
            int[] a = {-2, 1, -3, 4, -1, 2, 1, -5, 4 };
            FindMaxSumInSubArray max = new FindMaxSumInSubArray();
            int result = max.Solve(a);
            Assert.AreEqual(6, result);
        }
    }
}