﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace ArrayApplication.InterviewProblems.Tests
{
    [TestClass()]
    public class PrintAllSubSrrayWithZeroSumTests
    {
        [TestMethod()]
        public void PrintAllSubSrrayWithZeroSum_NaiveTest()
        {
            int[] arr = { 3, 4, -7, 3, 1, 3, 1, -4, -2, -2 };
            PrintAllSubSrrayWithZeroSum pas = new PrintAllSubSrrayWithZeroSum();
            List<List<int>> result = pas.PrintAllSubSrrayWithZeroSum_Naive(arr);

            Assert.AreEqual(6, result.Count);
        }
    }
}