﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ArrayApplication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayApplication.Tests
{
    [TestClass()]
    public class FindPairWithGivenSumInTheArrayTests
    {
        [TestMethod()]
        public void FindPair_NaiveTest()
        {
            int[] arr = { 8, 7, 2, 5, 3, 1 };
            FindPairWithGivenSumInTheArray fp = new FindPairWithGivenSumInTheArray();
            int[] result = fp.FindPair_Naive(arr, 10);
            Assert.AreEqual(2, result.Length);
            Assert.AreEqual(0, result[0]);
            Assert.AreEqual(2, result[1]);
        }

        [TestMethod()]
        public void FindPair_NLogNTest()
        {
            int[] arr = { 8, 7, 2, 5, 3, 1 };
            FindPairWithGivenSumInTheArray fp = new FindPairWithGivenSumInTheArray();
            int[] result = fp.FindPair_NLogN(arr, 10);
            Assert.AreEqual(2, result.Length);
            Assert.AreEqual(1, result[0]);
            Assert.AreEqual(5, result[1]);
        }

        [TestMethod()]
        public void FindPair_HashingTest()
        {
            int[] arr = { 8, 7, 2, 5, 3, 1 };
            FindPairWithGivenSumInTheArray fp = new FindPairWithGivenSumInTheArray();
            int[] result = fp.FindPair_Hashing(arr, 10);
            Assert.AreEqual(2, result.Length);
            Assert.AreEqual(0, result[0]);
            Assert.AreEqual(2, result[1]);
        }
    }
}