﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace StringInterviewPractice.InterviewProblems.Tests
{
    [TestClass()]
    public class LongestCommonSubstringTests
    {
        [TestMethod()]
        public void GetLongestCommonSubstringTest()
        {
            LongestCommonSubstring lcs = new LongestCommonSubstring();
            string s= lcs.GetLongestCommonSubstring("MicrosoftCorporationRedmond", "MSFTCORPRDMND");


            Assert.AreEqual("ftCorp", s);
        }
    }
}
