﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StringInterviewPractice.InterviewProblems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringInterviewPractice.InterviewProblems.Tests
{
    [TestClass()]
    public class RabinKarpTests
    {
        [TestMethod()]
        public void PatternSearchTest()
        {
            RabinKarp rk = new RabinKarp();
            // int n = rk.PatternSearch("ImtiazAlamgir", "FaiyazAlam");
            int n = rk.PatternSearch("abeda", "bed");

            Assert.AreEqual(1, n);
        }
    }
}